# Tracking todo tasks, bugs, ideas and other issues and notes

The order of the items may change, depending on the general priority/urgency: more important/recent ones are more at the top.  

----------------------------------------------------------------------------------------------------
**Several actions will (silently) fail at the moment if a MTP device is 'locked'**  
For example:  
- GetFunctionalObjects. (on a 'Samsung S4 mini')  
- getting the count of storage units (a storage is a functinal object...); will be 0. Compare with File Explorer.

Create something like bool isDeviceLocked()...

Update 2020-03/04: Does not seem to exist any special event (..._CAPABILITIES_UPDATE it is not).
But! When the smartphone is unlocked, WPD_EVENT_OBJECT_ADDED events are triggered for the (Functional)
Objects 'Phone' (s10001) and 'Card' (s20002).

So, filter: if WPD_EVENT_OBJECT_ADDED|REMOVED -> if Object is Functional -> if Fucntion is 'Storage':
Re-Scan...

2020-02-12
:    Created.

----------------------------------------------------------------------------------------------------
**More file/object operations...**  

- [ ] Copy folder (with content) from computer to device
- [ ] Copy folder (with content) from device to computer

- [ ] Copy file from device to computer

- [ ] Copy file                  on device from A to B (probably copy & delete)
- [ ] Copy folder (with content) on device from A to B on device (= probably copy & delete)

Use 'rename': Make it to a 'move' function, add a 'rename' as a shortcut (when used in the same folder).
- [ ] Move file                  on device from A to B (probably copy & delete)
- [ ] Move folder (with content) on device from A to B on device (= probably copy & delete)

----------------------------------------------------------------------------------------------------
**CMake: Check dependencies**  
`[2020-03-24 20:35]`

Building Tests does not, for example, copy a new version of the library into the test folder.

----------------------------------------------------------------------------------------------------
**Look into WPD_EVENT_DEVICE_REMOVED for details**

https://docs.microsoft.com/en-us/windows/win32/wpd_sdk/event-constants
> This event is sent when a driver for a device is being unloaded.
> Typically this is a result of the device being unplugged.
> Clients should release the IPortableDevice interface and any IPortableDeviceService
> interfaces specified in WPD_EVENT_PARAMETER_PNP_DEVICE_ID.

2020-04-21
  : Created.

----------------------------------------------------------------------------------------------------
**DeviceEvent: EventID and Details of last event are always missing**  
`[2020-01-12-001] [bug] [_]`

Tested it with CLI test app and doing multiple file deletion:  
Event notifications are printed, except for the final one.  
Played with different code (paths) already.

----------------------------------------------------------------------------------------------------
**Class Object: build|getFullDevicePath()**  
`[2020-01-12-002] [idea] [_]`

Device\Storage\ + build|getRelativeStoragePath()  
For 'Storage': getName(getFunctionalObjectID).Name?

----------------------------------------------------------------------------------------------------
**DeviceManager: std::vector<Device\*> -- why pointers?**
`[2019-12-20-001] [design] [_]`

First attempt to switch from pointers to (stack) object didn't work.
But didn't investigate any time yet for deeper analysis.

----------------------------------------------------------------------------------------------------
**getDevice(): FriendlyName is maybe not distinct enough**  
`[2019-12-20-002] [design] [_]`

But the 'int' type is already used by the overload for the index.
Other alternatives?

----------------------------------------------------------------------------------------------------
**Class Object: enumerate() könnte man ja prüfen, ob ObjProp gleich WPD_CONTENT_TYPE_FUNCTIONAL_OBJECT ist...**
... und falls ja: getFunctionalObjectData (abgeleitet wie oben) -- Update: Häh?

----------------------------------------------------------------------------------------------------
**countChildren (umbenennen und) in 'enumerate' einbauen?**

----------------------------------------------------------------------------------------------------
**Apropos 'enumerate': Wird zu oft genutzt (bzw. getObjectID). Furchtbar für die Performance.**
`2020-06-07`

Auch wenn man's threaden würde: Falscher Ansatz.
Sollte nur gemacht werden, wenn sich was signifikates am Inhalt ändert (add, delete, rename, usw.);
auf ein Signal/Event(?) dazu warten und erst dann den Inhalt erneut 'inventarisierten'.

Ansonsten auf den Cache (Tree?) zurückgreifen.

----------------------------------------------------------------------------------------------------
**Extend GuiTestApp to show object details (similar to Microsoft's "WPDInfo Tool" from the DDK)**

Detail Info Pane
```
	+-------------------------------------------------+---------------------+
	| [+] Samsung XY                                  | -Object Details-    |
	|       storage 1                                 | Filename:  sdsdsad  |
	|         Directory A      <---------------->     | Object ID: sdsadsad |
	|            File X                               | Type:      asdsadsa |
    |            ....                                 | ...                 |
	|         ...                                     |                     |
	| (device structure)                              |                     |
	+-------------------------------------------------+---------------------+
	| [Button] [Button] [Button] ...                                        |
	+-----------------------------------------------------------------------+
```

----------------------------------------------------------------------------------------------------
**Watch Target (Directory)-Funktionalität?!**

----------------------------------------------------------------------------------------------------
**Sollte der 'DeviceManager' ein Singleton werden?**

----------------------------------------------------------------------------------------------------
**Schreiben/Lesen von MTP-Geräten passiert nur seriell**

Dauert lange und resultiert ggf. oftmals in "is busy"-Meldungen, wenn anderweitige auf das Gerät
zugegriffen wird. Teilweise reicht es wohl schon aus, wenn man es auch nur parallel dazu in einem
Explorer-Fenster offen hat. Dann Warteschleife und später nochmal versuchen (Info aus Blog von 200x).

----------------------------------------------------------------------------------------------------
**IPortableDeviceManager::GetDevices(...) -> Fill list 1x.**  

Call ...::RefreshDeviceList() on arrival/removal (event notification)
https://docs.microsoft.com/de-de/windows/win32/api/portabledeviceapi/nf-portabledeviceapi-iportabledevicemanager-refreshdevicelist
> The best solution is to have the application register to get device arrival and removal
> notifications, and when a notification is received, have the application call this function.

----------------------------------------------------------------------------------------------------
**Split getPrintableInfo()**

Statt getPrintableInfo(), nach Bereich aufteilen ... in unterschiedliche `get...Info()`.

- [ ] DeviceManager
- [x] Device
- [ ] Storage (functional object)
- [ ] Object

----------------------------------------------------------------------------------------------------
**Device::emptyFolder() fails to delete a specific file**

2020-06-21

Taken from my empty testfiles, not write-protected, as far as I can tell, can be deleted by hand in the File Explorer.

"Neufassung von Text Document [2].mp3" -- the same/copy of that file is also in "Ordner 1" _and_
"Ordner 2" and both are deleted; so it shouldn't be a case of 'same ID'.

All other files and folder are deleted.
Worse: The feedback is still 0 (success), although one file was missing.
On the other hand, 1 (failure) is reported for non-existing folders...

Need to improve error handling/checking in Device::deleteObject().

----------------------------------------------------------------------------------------------------
**Implement Move operation**

2020-06-24

- https://docs.microsoft.com/en-us/windows/win32/wpd_sdk/moving-content-on-the-device
-   https://docs.microsoft.com/en-us/windows/win32/api/portabledeviceapi/nf-portabledeviceapi-iportabledevicecontent-move

Note that some devices may not support Move()
Just FYI, no real meat: https://freefilesync.org/forum/viewtopic.php?t=662

Check beforehand with SupportsCommand(), see MS link above.

----------------------------------------------------------------------------------------------------

## Misc. Tests with Android Smartphones

2020-06-29

The setup:

|        | Samsung Galaxy S4 mini | Motorola Moto E (2nd Gen.) | Samsung Galaxy A3 (2016)
|--------|------------------------|----------------------------|--------------------------
| Model: | GT-I9195               | XT1524                     | SM-A310F
| OS:    | Android 4.2.2          | Android 6.0                | Android 7.0

### Create a new folder with WPDLib, then copy MP3 file(s) into that new folder via WPDLib

|                                        | Samsung Galaxy S4 mini | Motorola Moto E (2nd Gen.) | Samsung Galaxy A3 (2016)
|----------------------------------------|------------------------|----------------------------|--------------------------
| Create a new folder with WPDLib:       | pass                   | pass                       | pass
| Copy MP3 file(s) into that new folder: | FAIL                   | FAIL                       | FAIL
| Empty that new folder:                 |                        |                            | FAIL

[FIXME] So, the WPDLib way of creating a folder misses something, because the folder is usuable when created 'by hand',
see next test.

### Create a new folder manually with the Windows File Explorer, then copy MP3 file(s) into that new folder via WPDLib

|         | Samsung Galaxy S4 mini | Motorola Moto E (2nd Gen.) | Samsung Galaxy A3 (2016)
|---------|------------------------|----------------------------|--------------------------
| Copy:   | pass                   | pass                       | pass
| Empty:  | FAIL                   | pass                       | FAIL

[TODO] Empty fails on 2 of 3; but works on the Moto E2...

### Copy MP3 files in existing folder on the device/storage ("Music")

|                           | Samsung Galaxy S4 mini | Motorola Moto E (2nd Gen.) | Samsung Galaxy A3 (2016)
|---------------------------|------------------------|----------------------------|--------------------------
| Copy:                     | pass                   | pass (1)                   | pass
| Empty:                    |                        |                            |
| TXT etc. and sub-folders: |                        |                            | pass
| MP3 files:                |                        |                            | FAIL

(1) But only the copy on the MotoE2 kept the M3 metadata (File properties -> Details).

____________________________________________________________________________________________________
                                                                                                 END