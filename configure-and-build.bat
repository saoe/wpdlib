@echo off
REM ---- Configure ---------------------------------------------------------------------------------
REM ~ Use the default compiler and linker of the system ~
REM cmake -S src -B _build

REM ~ Use Qt 5 from a specific path, install to a specific directory ~
cmake -D CMAKE_PREFIX_PATH=C:\devel\ext\Qt\5.14.1\_64DLL\5.14.1\msvc2017_64\lib\cmake -D CMAKE_INSTALL_PREFIX=C:\devel\shared -S src -B _build

REM ~ Use a specific version of Visual Studio ~
REM cmake -S src -B _build -G "Visual Studio 16 2019" -A x64

REM ~ Use the LLVM Clang compiler together with Visual Studio (as the build/link environment)
REM cmake -S src -B _build -G "Visual Studio 16 2019" -A x64 -T ClangCL

REM ---- Build -------------------------------------------------------------------------------------
REM ~ Build all (it's the same as with the option '--target ALL_BUILD') ~
REM cmake --build _build --config Release --parallel

REM ~ The 'Install' target depends on 'ALL_BUILD', and thus all projects that depend on the 'All' target will automatically also be built ~
cmake --build _build --target INSTALL --config Release --parallel
