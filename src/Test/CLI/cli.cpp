#include "WPDLib.h"

#include <chrono>
#include <iostream>
#include <thread>

#include <dbt.h> // For DBT_DEVICEARRIVAL etc.

class foo
{
	public:
		void test ()
		{
			std::cout << "xxxxxxxxx Foo - Event Test xxxxxxxxx";

		}
};

foo bar;


// [Win32 Message Loop] Callback function to handle the (broadcast) messages from Windows.

LRESULT CALLBACK WinProc (HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	PDEV_BROADCAST_HDR bchdr = (PDEV_BROADCAST_HDR) lparam;
	
	switch (msg)
	{
		case WM_DEVICECHANGE:
		{
			switch (wparam)
			{
				case DBT_DEVICEARRIVAL:
				{
					if (bchdr->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE)
					{
						PDEV_BROADCAST_DEVICEINTERFACE bcdi = (PDEV_BROADCAST_DEVICEINTERFACE)bchdr;

					}
					if (bchdr->dbch_devicetype == DBT_DEVTYP_PORT)
					{
						PDEV_BROADCAST_PORT bcp = (PDEV_BROADCAST_PORT)bchdr;
					}
					if (bchdr->dbch_devicetype == DBT_DEVTYP_VOLUME)
					{
						PDEV_BROADCAST_VOLUME bcv = (PDEV_BROADCAST_VOLUME)bchdr;
					}
					break;
				}

				case DBT_DEVICEREMOVECOMPLETE:
				{
					if (bchdr->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE)
					{
						PDEV_BROADCAST_DEVICEINTERFACE bcdi = (PDEV_BROADCAST_DEVICEINTERFACE)bchdr;
					}
					if (bchdr->dbch_devicetype == DBT_DEVTYP_PORT)
					{
						PDEV_BROADCAST_PORT bcp = (PDEV_BROADCAST_PORT)bchdr;
					}
					if (bchdr->dbch_devicetype == DBT_DEVTYP_VOLUME)
					{
						PDEV_BROADCAST_VOLUME bcv = (PDEV_BROADCAST_VOLUME)bchdr;
					}
					break;
				}
			}
			break;
		}
		
		default:
			return DefWindowProc(hwnd, msg, wparam, lparam);
	}

	return 0;
}


/*--------------------------------------------------------------------------------------------------
	Main part.
--------------------------------------------------------------------------------------------------*/

int main (int argc, char* argv [])
{
	// --- BEGIN --- [Win32 Message Loop: Create a window (invisible and only minimal), to get all messages from the system.
	HWND hwnd;
	
	LPCWSTR windowClassName = L"Window";
	WNDCLASS windowClass = {0};
	windowClass.lpfnWndProc = WinProc;
	windowClass.hInstance = NULL;
	windowClass.lpszClassName = windowClassName;
	if (!RegisterClass(&windowClass)) {	return 1; }
	hwnd = CreateWindow(windowClassName, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL);
	// --- END ---

	std::cout << "+++ WPDLib test application +++" << std::endl;
	
	// "Initializes the COM library for use [...] CoInitializeEx must be called at least once, and is usually called only once, for each thread that uses the COM library."
	HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
	
	// --------------------------------------------------------
	// Testing ground for all kinds of functionality...

	std::cout << "Scanning for devices..." << std::flush;
	//WPDLib::DeviceManager dm((HWND)0);
	WPDLib::DeviceManager dm(hwnd);
	dm.setEventCallback(WPDLib::DeviceEventType::WPD_EVENT_DEVICE_REMOVED, std::bind(&foo::test, &bar));
	
	std::cout << "\rScanning for devices - Done.\n\n" << std::flush;
	
	// --- #1: Loop over all found devices and do something to/on each device. ---
	
	/*
	for (std::vector<WPDLib::Device*>::iterator pos = dm.devices.begin(); pos != dm.devices.end(); ++pos)
	{
		// (*pos)->copyFileToDevice("Music\\1995 - Alice In Chains [cdrip]", "C:\\temp\\RF_TestFiles\\test-test-test-1\\WPD.txt", WPD_CONTENT_TYPE_GENERIC_FILE);
		//(*pos)->createFolder("Music\\1995 - Alice In Chains [cdrip]\\Level1\\Level2\\TEST");
		//(*pos)->renameObject("Music\\1995 - Alice In Chains [cdrip]\\WPD.txt", "_WPD-2.txt");
		//(*pos)->openWithShell((*pos)->getStorageName(0), "Music\\1995 - Alice In Chains [cdrip]\\Folder.jpg");
		//(*pos)->printTree((*pos)->tree.nodes);
		//std::cout << (*pos)->getPrintableInfo() << std::endl;
	}
	*/

	//// #1.1: Loop over all devices and storage-objects
	//for (;;)
	//{
	//	dm.scan(WPDLib::ScanMode::Basic);
	//	for (std::vector<WPDLib::Device*>::iterator pos = dm.devices.begin(); pos != dm.devices.end(); ++pos)
	//	{
	//		int s = (*pos)->getStorageCount();
	//		
	//		for (int j = 0; j < s; j++)
	//		{
	//			// std::cout << (*pos)->getFriendlyName() << " - " << (*pos)->getStorage(j).getName() << "\n" << std::flush;
	//
	//			std::cout << (*pos)->getPrintableInfo() << std::endl;
	//		}
	//	}
	//	std::cout << "-------------------------------------------------------------------------------\n";
	//	Sleep(3000);
	//}


	// --- #2: Pick a specific device and do something with/to it. ---

	// --- #2.1: Use index. Must be sure about the number, else it will crash/throw an execption.
	/*
	try
	{
		WPDLib::Device* d1 = dm.getDevice(0);
		std::cout << "Friendly Name: " << d1->getFriendlyName() << std::endl;
		std::cout << "Storage Name : " << d1->getStorageName(0) << std::endl;
		std::cout << std::endl;

		WPDLib::Device* d2 = dm.getDevice(1);
		std::cout << "Friendly Name: " << d2->getFriendlyName() << std::endl;
		std::cout << "Storage Name : " << d2->getStorageName(0) << std::endl;
		std::cout << std::endl;

		WPDLib::Device* d3 = dm.getDevice(2);
		std::cout << "Friendly Name: " << d3->getFriendlyName() << std::endl;
		std::cout << "Storage Name : " << d3->getStorageName(0) << std::endl;
		std::cout << std::endl;
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception catched: " << e.what() << std::endl;
	}
	*/

	// --- #2.2: Use the friendly name as a search argument.
	
	try
	{
		WPDLib::Device* d1 = dm.getDevice("Sascha Offe (GT-I9195)"); // Android 4.x
		WPDLib::Device* d2 = dm.getDevice("VOLUME"); // USB stick
        WPDLib::Device* d3 = dm.getDevice("MotoE2(4G-LTE)"); // Android 6.0
        WPDLib::Device* d4 = dm.getDevice("Galaxy A3 (2016)"); // Android 7.x
		
        //// std::string s("今天周五123äöÜß");
        //d1->getData();
        //d1->printTree(d1->tree.nodes);

        //d1->emptyFolder("Card\\RandFillMusic", true);
        //d1->copyFileToDevice("C:\\temp\\RF_TestFiles\\test-B\\Neufassung von Text Document [1].txt", "Phone", "RandFillMusic");
        //d1->copyFileToDevice("C:\\temp\\RF_TestFiles\\test-A\\Jack White - Lazaretto (2014)\\01 - Three Women.mp3", "Card", "RandFillMusic");

        std::string target_folder = "TEST_FOLDER";

        // --- Sascha Offe (GT-I9195) ---
        //d1->createFolder("Phone", target_folder);
        //d1->copyFileToDevice("C:\\temp\\RFTestFiles\\03. Un Autre Introduction.mp3", "Phone", target_folder);
        d1->emptyFolder("Phone", target_folder, true);
        
        
        // --- MotoE2(4G-LTE) ---
        //d3->createFolder("SD-Karte von SanDisk", target_folder);
        
        
        // --- Galaxy A3 (2016) ---
        //d4->createFolder("Phone", target_folder);
        
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception catched: " << e.what() << std::endl;
	}
	

	// --- #4: Do something to device no. 1 ---

	//dm.devices.at(0)->printTree(dm.devices.at(0)->tree.nodes);
	//dm.devices.at(0)->createFolder("Phone", "Music\\1995 - Alice In Chains [cdrip]\\Level1\\Level2\\TEST");
	//dm.devices.at(0)->createFolder("Card", "Music\\1995 - Alice In Chains [cdrip]\\Level1\\Level2\\TEST");

	//dm.devices.at(0)->openWithShell("Phone", "Music\\1995 - Alice In Chains [cdrip]\\Folder.jpg");
	
	//dm.devices.at(0)->createFolder("_TEST-TOP");
	//dm.devices.at(0)->createFolder("foo\\bar\\_BAZ");

	//dm.devices.at(0)->renameObject("Music\\1995 - Alice In Chains [cdrip]\\WPD.txt", "_WPD-2.txt");
	//dm.devices.at(0)->renameObject("Music\\1995 - Alice In Chains [cdrip]\\_TEST", "_TEST-2");
	
	//std::vector<std::string> paths { "Music\\1995 - Alice In Chains [cdrip]\\_TEST-2\\test-test-test-1\\file level 1 [3].txt",
	//                                 "Music\\1995 - Alice In Chains [cdrip]\\06. Again.mp3",
	//                                 "Music\\1995 - Alice In Chains [cdrip]\\07. Shame In You.mp3"
	//                               };

	//for (std::string x : paths)
	//	std::cout << x << std::endl; 
	
	//dm.devices.at(0)->deleteObjects("Phone", paths);
		

	// --- BEGIN --- Loop to catch and process the (broadcast) messages from Windows.
	std::cout << "(Press CTRL+C to stop)\n" << std::flush;

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	// --- END ---
	
	if (hr >= 0)
		CoUninitialize();

	std::cout << "Done!" << std::endl;

	return 0;
}