#ifndef LOGWIDGET_H
#define LOGWIDGET_H

#include <QPlainTextEdit>
#include <QFileSystemWatcher>

//class QFile;

class LogWidget : public QPlainTextEdit
{
	Q_OBJECT

	public:
		static const QString stdout_filepath;
		
		LogWidget ();
		~LogWidget ();
		void appendMessage (const QString& text);
	
	private:
		QFileSystemWatcher* watcher;
		FILE*               f;
		
		QString timestamp ();

	public slots:
		void onSaveLogToFile ();
		void onFileChanged ();
		void onClearLogWidget ();
};

#endif