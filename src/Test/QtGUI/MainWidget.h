#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include "LogWidget.h"
#include "Model.h"

#include <QPushButton>
#include <QStatusBar>
#include <QTreeView>
#include <QWidget>

class DeviceScanner;

/* -------------------------------------------------------------------------------------------------
Class declaration and method definitions for the application main window.
*/

class MainWidget : public QWidget
{
	Q_OBJECT

	public:
		LogWidget log_window;
		QStatusBar status_bar;
		QTreeView* tree_view;
		WPDLib::Model* model;

		MainWidget ();
		~MainWidget ();
		bool nativeEvent (const QByteArray &eventType, void* message, long* result);
	
	public slots:
		void onScanningDone ();
		void ChangeNotification ();
		void onScanDevices ();
		void onDeviceArrival ();
		void onDeviceRemoveComplete ();
		void onStartDeviceScan ();
	
	signals:
		void DeviceArrival ();
		void DeviceRemoveComplete ();
		void startDeviceScan ();

	private:
		QPushButton* button_scanDevices;
		QPushButton* button_saveLogToFile;
		QPushButton* button_clearLogWidget;

		QThread* devicescan_thread;
		DeviceScanner* scanner;
};

#endif