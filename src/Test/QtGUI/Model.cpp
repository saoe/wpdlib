﻿/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2018-2019 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT
*/

#include "Model.h"
#include "Device.h"
#include <iostream>

namespace WPDLib
{

Model::Model ()
{
	/* empty */
}


Model::Model (Device* dev, Tree t, Model* m)
{
	this->tree = t;

	setHorizontalHeaderLabels(QStringList("Header Text"));

	QStandardItem* parent;

	if (m)
		parent = m->invisibleRootItem();
	else
		parent = this->invisibleRootItem();

	setup(dev, t.nodes, parent);
}


Model::~Model ()
{

}


void Model::setup (Device* device, std::vector<Tree::Node> x, QStandardItem* parent)
{
	for (std::vector<Tree::Node>::iterator i = x.begin(); i != x.end(); ++i)
	{
		QStandardItem* item = new QStandardItem();
		
		item->setData(QString::fromStdString(i->data.getLabel()), Qt::DisplayRole);
		item->setEditable(false);
		
		parent->appendRow(item);
		setup(0, i->children, item);
	}
}


} // namespace WPDLib.