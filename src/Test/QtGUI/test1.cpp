/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2015-2018 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT
*/

#include "MainWidget.h"

#include <QApplication>
#include <QScreen>

int main (int argc, char* argv[])
{
	QApplication app(argc, argv);

	// Setup the main application window.
	QScreen* screen = QGuiApplication::primaryScreen();
	QRect screenGeometry = screen->geometry();
	MainWidget* window = new MainWidget();
	window->resize(screenGeometry.width()/2, screenGeometry.height()/2);

	window->show();

	return app.exec();
}
