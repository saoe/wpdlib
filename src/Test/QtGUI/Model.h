﻿/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2018 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT

Implemented with the help of (among others):
- "Qt Documentation: Simple Tree Model Example" <http://doc.qt.io/qt-5/qtwidgets-itemviews-simpletreemodel-example.html>
- "Meeting C++: Trees, tree models and tree views in Qt" <https://www.meetingcpp.com/blog/items/trees-treemodels-and-treeviews-in-qt.html>

*/

#ifndef WPDLIB_QTGUI_MODEL_H
#define WPDLIB_QTGUI_MODEL_H

#include <QStandardItemModel>
#include "Tree.h"

namespace WPDLib
{

class Device;

class Model : public QStandardItemModel
{
	Q_OBJECT

	public:
		         Model ();
		explicit Model (Device* dev, Tree t, Model* m = nullptr);
		         ~Model ();

		Tree tree;

	private:
		void setup (Device* dev, std::vector<Tree::Node> x, QStandardItem* parent);
};

} // namespace WPDLib

#endif