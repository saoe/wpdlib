#include "LogWidget.h"

#include <QDateTime>
#include <QFileDialog>
#include <QMutex>
#include <QStandardPaths>
#include <QTextStream>
#include <QScrollBar>

extern QMutex file_lock;

const QString LogWidget::stdout_filepath = QStandardPaths::writableLocation(QStandardPaths::TempLocation) + "/" + "wpdlib-test-stdout.txt";

LogWidget::LogWidget ()
{
	// Redirect console output to a file.
	f = freopen(stdout_filepath.toStdString().c_str(), "w", stdout);

	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	setReadOnly(true);
	setLineWrapMode(QPlainTextEdit::NoWrap);
	
	QFont font("Consolas", 10);
	font.setStyleHint(QFont::Monospace);
	setFont(font);

	watcher = new QFileSystemWatcher(this);
	watcher->addPath(stdout_filepath);
	connect(watcher, SIGNAL(fileChanged(const QString&)), this, SLOT(onFileChanged()));
}


LogWidget::~LogWidget ()
{
	fclose(stdout);
	fclose(f);
	remove(stdout_filepath.toStdString().c_str());
}


void LogWidget::appendMessage (const QString& text)
{
	appendPlainText("[" + timestamp() + "] " + text);
	verticalScrollBar()->setValue(verticalScrollBar()->maximum()); // Scroll to the bottom.
}


QString LogWidget::timestamp ()
{
	return QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz");
}


void LogWidget::onSaveLogToFile ()
{
	QDateTime timestamp = QDateTime::currentDateTime();

	QFile file(QFileDialog::getSaveFileName(this,
	                                        "Save File",
	                                        QStandardPaths::writableLocation(QStandardPaths::DesktopLocation) + "/" + "logfile_"+timestamp.toString("yyyy-MM-dd")+".txt",
	                                        tr("Text file (*.txt)")));
	
	QIODevice::OpenMode mode;
	
	file.exists() ?	mode = QIODevice::Append    | QIODevice::Text
	              :	mode = QIODevice::WriteOnly | QIODevice::Text;
	
	file.open(mode);
	
	QTextStream writer(&file);
	
	writer << this->toPlainText();
    
	file.flush();
    file.close();
}


void LogWidget::onFileChanged ()
{
	file_lock.lock();
	QFile stdout_file(stdout_filepath);
	stdout_file.open(QIODevice::ReadOnly | QIODevice::Text);
	QTextStream ReadFile(&stdout_file);
	file_lock.unlock();
	appendMessage(ReadFile.readAll());
}

void LogWidget::onClearLogWidget ()
{
	this->clear();
}