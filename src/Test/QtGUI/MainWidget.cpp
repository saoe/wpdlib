#include "MainWidget.h"
#include <QLabel>
#include <QLayout>
#include <QMutex>
#include <QSplitter>
#include <QThread>
#include <windows.h>
#include <dbt.h>
#include <iostream>
#include "WPDLib.h"


QMutex file_lock; // Lock for the console-output-to-file.


// -------------------------------------------------------------------------------------------------
// https://mayaposch.wordpress.com/2011/11/01/how-to-really-truly-use-qthreads-the-full-explanation/
// http://blog.qt.io/blog/2010/06/17/youre-doing-it-wrong/

class DeviceScanner : public QObject
{
	Q_OBJECT

	public:
		DeviceScanner (MainWidget* m_w)
		{
			mw = m_w;
		}
		
		//~DeviceScanner () {};

	public slots:
		
		// What will be executed when the thread is started.
		void scan ()
		{
			// "Initializes the COM library for use [...] CoInitializeEx must be called at least once, and is usually called only once, for each thread that uses the COM library."
			// Don't put it in something like DllMain (big no-no!).
			// Won't work in the class constructor either: The thread does not yet exist when the constructor is executed!
			hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
	
			file_lock.lock();

			// --------------------------------------------------------
			// Testing ground for all kinds of functionality...

			WPDLib::DeviceManager dm((HWND)0);
			//WPDLib::DeviceManager dm((HWND)mw->winId());

			dm.scan(WPDLib::ScanMode::Full);
			model = new WPDLib::Model();

			for (std::vector<WPDLib::Device*>::iterator pos = dm.devices.begin(); pos != dm.devices.end(); ++pos)
			{
				//(*pos)->copyFileToDevice("Music\\1995 - Alice In Chains [cdrip]", "C:\\temp\\RF_TestFiles\\test-test-test-1\\WPD.txt", WPD_CONTENT_TYPE_GENERIC_FILE);
				//(*pos)->createFolder("Music\\1995 - Alice In Chains [cdrip]", "_TEST");
				//std::cout << (*pos)->getPrintableInfo() << std::endl;
				
				// new?
				WPDLib::Model(*pos, (*pos)->tree, model);
				
				//(*pos)->printTree((*pos)->tree.nodes, 1);
			}

			mw->tree_view->setModel(model);

			// --------------------------------------------------------

			file_lock.unlock();	

			if (hr >= 0)
				CoUninitialize();

			emit finished();
		}

	signals:
		void finished ();

	private:
		HRESULT hr;
		MainWidget* mw;
		WPDLib::Model* model;
};



MainWidget::MainWidget ()
{
	// Set up the worker thread and let it do the work.
	devicescan_thread = new QThread;
	scanner = new DeviceScanner(this);
	scanner->moveToThread(devicescan_thread);

	setAttribute(Qt::WA_DeleteOnClose); // Since this widget doesn't have a parent, make sure that all deletion/deconstruction still takes place.
	
	// Set up the GUI (widgets and layout).
	setWindowTitle("QtGUITestApp for WPDLIib");
	setWindowIcon(QIcon(":/WPDLib.ico"));
		
	QVBoxLayout* layout = new QVBoxLayout;
	QHBoxLayout* buttons = new QHBoxLayout;
	button_scanDevices = new QPushButton(tr("Scan Devices"), this);
	button_scanDevices->setDisabled(true);
	button_saveLogToFile = new QPushButton(tr("Save Log To File..."), this);
	button_clearLogWidget = new QPushButton(tr("Clear Log"), this);

	buttons->addWidget(button_scanDevices);
	buttons->addWidget(button_saveLogToFile);
	buttons->addWidget(button_clearLogWidget);

	QSplitter* splitter = new QSplitter(Qt::Vertical);

	tree_view = new QTreeView(splitter);

	tree_view->setAlternatingRowColors(true);
	tree_view->setHeaderHidden(true);
	//tree_view->expandAll();
	
	splitter->addWidget(tree_view);
	splitter->addWidget(&log_window);
	layout->addWidget(splitter);

	layout->addLayout(buttons);
	layout->addWidget(&status_bar);
	setLayout(layout);

	// Set up the signal/slot connections.
	connect(button_scanDevices, SIGNAL(clicked(bool)), this, SLOT(onScanDevices()));
	connect(button_saveLogToFile, SIGNAL(clicked(bool)), &log_window, SLOT(onSaveLogToFile()));
	connect(button_clearLogWidget, SIGNAL(clicked(bool)), &log_window, SLOT(onClearLogWidget()));

	connect(this, SIGNAL(DeviceArrival()), this, SLOT(onDeviceArrival()));
	connect(this, SIGNAL(DeviceRemoveComplete()), this, SLOT(onDeviceRemoveComplete()));
	connect(this, SIGNAL(startDeviceScan()), this, SLOT(onStartDeviceScan()));

	connect(devicescan_thread, SIGNAL(started()), scanner, SLOT(scan()));
	connect(scanner, SIGNAL(finished()), devicescan_thread, SLOT(quit()));
	connect(scanner, SIGNAL(finished()), this, SLOT(onScanningDone()));

	// Trigger first device scan.
	emit startDeviceScan();
	status_bar.showMessage(tr("Please wait: Scanning for devices..."));
}


MainWidget::~MainWidget ()
{
	delete scanner;
	delete devicescan_thread;
}


/* -------------------------------------------------------------------------------------------------
Reimplemented special event handler to receive native platform events (in this case, the Windows Message Loop).
*/

bool MainWidget::nativeEvent (const QByteArray &eventType, void* message, long* result)
{
	MSG *msg = static_cast<MSG*>(message);

	if (msg->message == WM_DEVICECHANGE)
	{
		switch (msg->wParam)
		{
			case DBT_DEVICEARRIVAL:
			{
				emit DeviceArrival();
				break;
			}
			
			case DBT_DEVICEREMOVECOMPLETE:
			{
				emit DeviceRemoveComplete();
				break;
			}
		}
		return true;
	}
	return false;
}


void MainWidget::onScanDevices ()
{
	emit startDeviceScan();
}


void MainWidget::onScanningDone ()
{
	log_window.onFileChanged();
	status_bar.showMessage(tr("Scanning done."), 5000);
	button_scanDevices->setEnabled(devicescan_thread->isFinished());
}


void MainWidget::onStartDeviceScan ()
{
	status_bar.showMessage(tr("Please wait: Scanning for devices..."));
	log_window.appendMessage("Scanning for devices...");
	
	button_scanDevices->setEnabled(devicescan_thread->isFinished());
	devicescan_thread->start();

	/* Of course, this seems unelegant to simply sleep/wait for an arbitrary amount of time...
	Unfortunately, while testing, it occured very often, that WPD and some devices (USB stick) don't seem to get along very well.
	After plugging it in, all 'arrival' events and signals happen, and the Explorer also shows the device instantly.
	But immediately triggering the scan here won't find the device (most of the time!).
	But if you click the scan button a few seconds later: No problem.
	So there seems to be a bit of a timing issue going on. */
	devicescan_thread->sleep(5); // Seconds.
}


void MainWidget::ChangeNotification ()
{
	log_window.onFileChanged();
}


void MainWidget::onDeviceArrival ()
{
	log_window.appendMessage("Device Arrival.");
	emit startDeviceScan();
}


void MainWidget::onDeviceRemoveComplete ()
{
	log_window.appendMessage("Device Remove Complete.");
	emit startDeviceScan();
}

#include "MainWidget.moc"