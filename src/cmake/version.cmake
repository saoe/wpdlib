# ╭────────────────────────────────────────────────────────────────────────────────────────────────╮
# │                           Windows Portable Devices Library (WPDLib)                            │
# ╰────────────────────────────────────────────────────────────────────────────────────────────────╯
# Copyright © 2015, 2019-2020 Sascha Offe <so@saoe.net>
# SPDX-License-Identifier: MIT

# Timestamp of the build (local time; append the optional parameter 'UTC' for Coordinated Universal Time).
#
#string (TIMESTAMP BUILDTIME "%Y-%m-%d %H:%M:%S")

find_package(Git)

# Note that the following expects a specific directory structure; so adapt to your own style if required:
#
# Project/
# +-- .git/
# +-- src/
#     +--  CMakeLists.txt   <-- top-level CMake file
#
get_filename_component (ONE_DIRECTORY_ABOVE_TOPLEVELDIR ${CMAKE_SOURCE_DIR}/.. ABSOLUTE)

# Get full length SHA-1 Git commit hash of current 'HEAD' revision, the unique identifier of the pointer to the current local branch.
#
if (GIT_FOUND AND EXISTS "${ONE_DIRECTORY_ABOVE_TOPLEVELDIR}/.git")
	execute_process (
		 COMMAND         ${GIT_EXECUTABLE} rev-parse HEAD
		 OUTPUT_VARIABLE GIT_COMMIT_HASH
		 OUTPUT_STRIP_TRAILING_WHITESPACE
	)
endif ()

# Substitute the placeholders with real values and create a usable header file.
#
configure_file (${IN} ${OUT} @ONLY)
