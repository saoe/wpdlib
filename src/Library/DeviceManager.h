/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2015-2020 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT

Interface of the class 'WPDLib::DeviceManager'.

Since multiple WPD-compatible devices can be plugged in to a computer at the same time, this class
acts as the 'middleman/mediator/broker' that organsises the basic functions like selecting one of
the devices for any following action or to get an overview of it.
*/

#ifndef WPDLIB_DEVICEMANAGER_H
#define WPDLIB_DEVICEMANAGER_H

#include <Windows.h>
#include <functional>
#include <string>
#include <vector>

#ifdef DLL_EXPORT
	#define DLL __declspec(dllexport)
#else
	#define DLL __declspec(dllimport)
#endif


class IPortableDeviceManager;
class IPortableDeviceValues;

namespace WPDLib
{

class Device;
enum class ScanMode;
enum class DeviceEventType;

class DLL DeviceManager
{
	public:
		DeviceManager (HWND hwnd = 0);
		~DeviceManager ();

		void scan (ScanMode sm);
		int  countDevices () const;

		Device* getDevice (int index);
		Device* getDevice (std::string FriendlyName);

		std::vector<Device*> devices;
			// [TODO] Pointer! => cpyctor/assignop!
			// [TODO] Move to implementation details (private)? But currently still needed by clients for iteration loops.

		void setEventCallback (DeviceEventType e, std::function<void()> callback);

	private:
		IPortableDeviceManager* ipdm;
		IPortableDeviceValues*  client_information;
		HDEVNOTIFY              device_notification_handle_usb,
		                        device_notification_handle_wpd;

		void registerDeviceNotifications (HWND hwnd);
		void unregisterDeviceNotifications ();
		void addDeviceToList (Device*);
		void removeDeviceFromList (std::string FriendlyName);
		void clearDevicesList ();

		/* ???
		write_data_to_device
		*/
};

} // namespace WPDLib

#endif
