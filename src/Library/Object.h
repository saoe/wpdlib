/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2016-2020 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT

Interface of the class 'WPDLib::Object'.
This class represents a generic WPD object (e.g. an item in the filesystem of a WPD device).
*/

#ifndef WPDLIB_OBJECT_H
#define WPDLIB_OBJECT_H

#include <Windows.h>
#include <string>

#ifdef DLL_EXPORT
	#define DLL __declspec(dllexport)
#else
	#define DLL __declspec(dllimport)
#endif

class IPortableDeviceProperties;

namespace WPDLib
{

class DLL Object
{
	public:
		Object (IPortableDeviceProperties* const Properties, std::string object_id);
		Object ();
		
		std::string        getFullPath () const;
		std::string        getRelativeStoragePath () const;
		std::string        getParentID () const;
		std::string        getID () const;
		std::string        getPersistentUniqueID () const;
		std::string        getName () const;
		void               setName (std::string s);
		std::string        getOriginalFileName () const;
		std::string        getLabel () const;
		CLSID              getContentType () const;
		CLSID              getFormat () const;
		unsigned long long getSizeInBytes () const;
        std::string        getCreationTimeAsString () const;
        std::string        getAuthorTimeAsString () const;
        std::string        getModificationTimeAsString () const;
		bool               isContentType (CLSID ct) const;
		bool               isFormat (CLSID f) const;
		bool               isFolder () const;
		bool               isFunctionalObject () const;

	private:		
		// Common WPD Object Properties:
		// -----------------------------

		/* A string ID that uniquely identifies the object on the device (for the duration of the connection!).
		   This ID need not be stored across sessions. 
		   If this property is both unique and persistent, the driver may set both WPD_OBJECT_PERSISTENT_UNIQUE_ID
		   and WPD_OBJECT_ID to the same value. */
		std::string ID;                          // WPD_OBJECT_ID

		/* A string ID that uniquely identifies the object on the device, similar to WPD_OBJECT_ID,
		   but it must be stored across sessions.
		   If the object identifier WPD_OBJECT_ID is both unique and persistent, the driver may set both
		   WPD_OBJECT_PERSISTENT_UNIQUE_ID and WPD_OBJECT_ID to the same value. */
		std::string PersistentUniqueID;          // WPD_OBJECT_PERSISTENT_UNIQUE_ID	

		/* The object ID of the parent object.
		   The only object that can return an empty string for this value is the root device object.
		   To modify this property a client would call IPortableDevice::SendCommand(WPD_COMMAND_STORAGE_MOVE). */
		std::string ParentID;                    // WPD_OBJECT_PARENT_ID

		/* The object ID of the closest functional object containing this object.
		   For example, a file inside a storage functional object will have this property set to the ID of the storage functional object.
		   In short: Usually (in generic cases) the object ID of the storage on which the file/folder resides. */
		std::string ContainerFunctionalObjectID; // WPD_OBJECT_CONTAINER_FUNCTIONAL_OBJECT_ID

		// An opaque string created by a client to retain state between sessions without retaining a catalogue of connected device content.
		std::string SyncID;                      // WPD_OBJECT_SYNC_ID

		/* The display(!) name for the object.
		   Note: There are weird automatism at work: ".text" is handled as a file extension and might be cut off.
		         Example: A folder is named "Great Album Vol. 2 (2011)" (= WPD_OBJECT_ORIGINAL_FILE_NAME),
		         but the Name (= WPD_OBJECT_NAME) is cut to just "Great Album Vol" -- as if '. 2 (2011)' was a
				 file extension that the user doesn't need to know... */
		std::string Name;                        // WPD_OBJECT_NAME

		// A string name for the file.
		std::string OriginalFileName;            // WPD_OBJECT_ORIGINAL_FILE_NAME

		// The size of the object resource data in bytes.
		unsigned long long SizeInBytes;          // WPD_OBJECT_SIZE

		/* A GUID identifying the generic type of this object.
		   For example, a document or e-mail.
		   This can be an object type defined by Windows Portable Devices, or a custom driver content type.
		   The device object is the only object that does not report this property. */
		CLSID ContentType;                       // WPD_OBJECT_CONTENT_TYPE

		/* A GUID identifying the format of the object data.
		   This can be a format defined by Windows Portable Devices, or a custom driver format. */
		CLSID Format;                            // WPD_OBJECT_FORMAT

		// String containing a list of space-delimited keywords associated with this object.
		std::string Keywords;                    // WPD_OBJECT_KEYWORDS

		// Indicates the date and time the object was created on the device.
        // NOTE: This a string for displaying, not a value for further calculations!
		std::string DateCreated;                 // WPD_OBJECT_DATE_CREATED

		// Indicates the date and time the object was modified on the device.
        // NOTE: This a string for displaying, not a value for further calculations!
		std::string DateModified;                     // WPD_OBJECT_DATE_MODIFIED

		/* Indicates the date and time that the content was created.
		   This may not be the same as the creation date of the file.
		   For example, a music file from would have an authoring date of when the music was recorded,
		   but a creation date of when the WMA file was actually created on the device. */
        // NOTE: This a string for displaying, not a value for further calculations!
		std::string DateAuthored;                     // WPD_OBJECT_DATE_AUTHORED

		/* Indicates whether the thumbnail image for this object should be created from the default resource data.
		   This provides a way for objects without a thumbnail resource to provide a friendlier browsing experience.
		   Using this flag may affect the first display response, since the application must retrieve and calculate
		   a thumbnail image from the device; it is more efficient to provide a separate thumbnail image resource, if possible. */
		bool GenerateThumbnailFromResource;      // WPD_OBJECT_GENERATE_THUMBNAIL_FROM_RESOURCE

		// Indicates whether the media data is DRM-protected.
		// If not present, this is assumed to be False.
		bool IsDRMProtected;                     // WPD_OBJECT_IS_DRM_PROTECTED

		// Indicates whether the object should be hidden.
		// If not present, the object is assumed to be not hidden.
		bool IsHidden;                           // WPD_OBJECT_ISHIDDEN

		// Indicates whether the object can be deleted, or not.
		bool IsDeletable;                        // WPD_OBJECT_CAN_DELETE

		// Indicates whether the object represents system data (such as a system file).
		// If not present, the object is assumed to be not a system object.
		bool IsSystem;                           // WPD_OBJECT_ISSYSTEM

		// Determines whether this object is intended to be understood or merely stored by the device.
		// If this property is not present, all data is assumed to be intended for consumption.
		bool IsNonConsumable;                   // WPD_OBJECT_NON_CONSUMABLE

		// An IPortableDevicePropVariantCollection containing a collection of VT_LPWSTR object IDs identifying the referenced objects.
		// This is required only if the object is a reference object, such as a folder or playlist.
		IUnknown* References;                   // WPD_OBJECT_REFERENCES (VT_UNKNOWN)

		/* The latter half of the full path as shown in the file explorer on the computer (excluding host, MTP device and storage).
		   Example: Computer\Samsung GT-I9195\Phone\DCIM\Camera\20140416_193559.jpg
		                                            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Relative Path: DCIM\Camera\20140416_193559.jpg */
		std::string RelativeStoragePath;

		std::string getOriginalFileName (IPortableDeviceProperties* Properties, std::string ObjectID);
		std::string buildRelativeStoragePath (IPortableDeviceProperties* Properties, std::string ObjectID);
};

} // namespace WPDLib

#endif
