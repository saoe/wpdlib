/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2016, 2018-2021 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT

Miscellaneous helper functions.
*/

#ifndef WPDLIB_UTIL_H
#define WPDLIB_UTIL_H

#include <iomanip>
#include <sstream>
#include <tuple>
#include <vector>

#ifdef DLL_EXPORT
	#define DLL __declspec(dllexport)
#else
	#define DLL __declspec(dllimport)
#endif


namespace WPDLib
{

namespace util
{

DLL std::string                          narrow (const std::wstring&);
DLL std::wstring                         widen (const std::string&);
DLL std::vector<std::string>             splitPath (std::string path);
DLL std::tuple<std::string, std::string> splitStorageAndPath (std::string full_path);
DLL std::string                          GUIDToString (GUID* guid);
DLL GUID                                 StringToGUID (const std::string& str);
DLL char                                 findFirstDriveFromMask (ULONG bitmask);

// [IMPORTANT] This code has been taken from my project/repository "The Nifty Oddity Toolkit.
//             If you make any general/not project-specific modifications here, please also apply it there.
template <typename T>
std::string NumberToString (T number)
{
	std::ostringstream oss;
	oss << number;
	return oss.str();
}


/*--------------------------------------------------------------------------------------------------
Returns a nice-looking text string for those big bytes numbers.
Automatically calculates the best fitting size for the amount of bytes.

[IMPORTANT] This code has been taken from my project/repository "The Nifty Oddity Toolkit.
            If you make any general/not project-specific modifications here, please also apply it there.
--------------------------------------------------------------------------------------------------*/

template <typename T>
std::string translateBytesToHandySizeText (T bytes_input)
{	
	std::vector<std::string> storage_unit = {"Bytes",
	                                         "Kilobytes",
	                                         "Megabytes",
	                                         "Gigabytes",
	                                         "Terabytes",
	                                         "Petabytes",
	                                         "Exabytes"  };

	int   i = 0;
	float t = bytes_input;

	while (t >= 1024 && i < storage_unit.size())
	{
		t /= 1024;
		i++;
	}

	std::stringstream ss;
	ss << std::fixed << std::setprecision(2) << t;
	return ss.str() + " " + storage_unit.at(i);
}


template <typename T>
void translateBytesToHandySizeText (T bytes_input, char* destination, size_t n)
{
	std::string s = translateBytesToHandySizeText(bytes_input);
	strncpy(destination, s.c_str(), n-1);
	destination[n-1] = '\0';
}

} // namespace util

} // namespace WPDLib

#endif
