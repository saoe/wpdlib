/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2015-2020 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT

Implementation of the class 'WPDLib::DeviceEvent'.

Based on code and information from:
- Listening to WPD Events <http://blogs.msdn.com/b/dimeby8/archive/2006/10/06/listening-to-wpd-events.aspx>
- Listening to MTP events <http://blogs.msdn.com/b/dimeby8/archive/2006/10/06/listening-to-mtp-events.aspx>

Note that above text is from 2006 and MSDN differs slightly (e.g. IPortableDevice::Advise():
"pParameters [in]: This parameter is ignored and should be set to NULL.")
- Handling Events from the Device <https://msdn.microsoft.com/en-us/library/windows/desktop/dd319342%28v=vs.85%29.aspx>

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

Another interesting observation and 'confirmed' by this thread on Stack Overflow and my tests:

Although I did get events by my MTP device (smartphone), when I renamed a file on it via Explorer,
no events were fired when I did the same on a USB stick.

But when I renamed the file on the USB stick via a 'WPD method' (-> WPDLib), then WPD_EVENT_OBJECT_*
events were triggered. (The mentioned filesystem minifilters are way too much work.)

From https://stackoverflow.com/questions/42648329/windows-portable-device-notify-when-a-new-file-is-created-copied-deleted:
> Is it possible to be notified when a file is deleted from / copied to / storage device using (IPortableDeviceEventCallback).
> So far i've only been notified when the flash drive was disconnected or its name was changed externally by the user.
> Edit: I'm interested in receiving the WPD_EVENT_OBJECT_ADDED event.
> IPortableDeviceCapabilities::GetSupportedEvents shows that the needed event is supported, but nothing is sent upon new file copy
> - igal k
> 
>> WPD_EVENT_OBJECT_ADDED corresponds to MTP event ObjectAdded.
>> I think that what events are really sent depends on concrete implementation of MTP protocol on concrete portable device.
>> I beleave that WPD API main purpose is to manage portable devices like a phones and music players, access and methods to
>> work with usb flash drives are only exported as some layer of compatibility, and may not contain all functions.
>> If you want to receive such notifications not from portable devices implements MTP, but only from USB flash drives,
>> you should take a look for example on filesystem minifilters. They could give you needed callbacks.
>> - Arthur Bulakaiev (2017-03-07)
>> 
>>> As my "Edit" Snippet states, the flash driver does support the Object_Added event.
>>> so how come it never sends it?
>>> – igal k (2017-03-08)
>>> 
>>> As far as "supporting" means here only some layer of abstraction, I think it depends on how are you testing it.
>>> I have tried such case on MS WPD API sample with my flash disk, and it seems that WPD events are fired only
>>> when you working with it's content with WPD methods: transfering content through IPortableDevice methods, and so on.
>>> But there are no events from operations with that flash as usb storage device, such as creating folder from Explorer, etc.
>>> – Arthur Bulakaiev (2017-03-09)
*/

#include "DeviceEvent.h"

namespace WPDLib
{

/*--------------------------------------------------------------------------------------------------
Construtor
--------------------------------------------------------------------------------------------------*/

DeviceEvent::DeviceEvent (Device* d) : ref_count(1)
                                     , event_cookie(NULL)
                                     , device(d)
	                                 , event_callback_device_capabilities_updated(nullptr)
	                                 , event_callback_device_removed(nullptr)
	                                 , event_callback_device_reset(nullptr)
	                                 , event_callback_object_added(nullptr)
	                                 , event_callback_object_removed(nullptr)
	                                 , event_callback_object_transfer_requested(nullptr)
	                                 , event_callback_object_updated(nullptr)
	                                 , event_callback_service_method_complete(nullptr)
	                                 , event_callback_storage_format(nullptr)
{
	/* empty */
}


/*--------------------------------------------------------------------------------------------------
Destructor
--------------------------------------------------------------------------------------------------*/

DeviceEvent::~DeviceEvent ()
{
	/* empty */
}


/*--------------------------------------------------------------------------------------------------
COM stuff...
--------------------------------------------------------------------------------------------------*/

HRESULT DeviceEvent::QueryInterface (REFIID riid, LPVOID* ppvObj)
{
	HRESULT hr = S_OK;
		
	if (ppvObj == NULL)
	{
		hr = E_INVALIDARG;
		return hr;
	}

	if ((riid == IID_IUnknown) || (riid == IID_IPortableDeviceEventCallback))
	{
		AddRef();
		*ppvObj = this;
	}
	else
	{
		hr = E_NOINTERFACE;
	}
    
	return hr;
}


/*--------------------------------------------------------------------------------------------------
COM stuff...
--------------------------------------------------------------------------------------------------*/

ULONG DeviceEvent::AddRef ()
{
	InterlockedIncrement((long*) &ref_count);
	return ref_count;
}


/*--------------------------------------------------------------------------------------------------
COM stuff...
--------------------------------------------------------------------------------------------------*/

ULONG DeviceEvent::Release ()
{
	ULONG ret_val = ref_count - 1;

	if (InterlockedDecrement((long*) &ref_count) == 0)
	{
		delete this;
		return 0;
	}

	return ret_val;
}


/*--------------------------------------------------------------------------------------------------
Register for device events.

Return values: S_OK if registration was successful.
               S_FALSE if registration failed.
--------------------------------------------------------------------------------------------------*/

HRESULT DeviceEvent::Register (IPortableDevice* device)
{
	// Check to see if we already have an event registration cookie.  If so, then avoid registering again.
	// An application can register for events as many times as they want.
	// We only keep a single registration cookie around for simplicity.
	if (event_cookie != NULL)
	{
		return S_FALSE;
	}

	HRESULT hr;

	// IPortableDevice::Advise is used to register for event notifications
	// This returns a cookie (string) that is needed while unregistering.
	hr = device->Advise(0, this, 0, &event_cookie);
	return hr;
}


/* -------------------------------------------------------------------------------------------------
Unregister from device event notification.
--------------------------------------------------------------------------------------------------*/

HRESULT DeviceEvent::Unregister(IPortableDevice* device)
{
	// Return S_OK if we are not registered for any thing.
	if (event_cookie == NULL)
	{
		return S_OK;
	}

	// IPortableDevice::Unadvise is used to stop event notification.
	// We use the cookie (string) that we received while registering.
	HRESULT hr = device->Unadvise(event_cookie);

	// Free string allocated earlier by Advise call.
	CoTaskMemFree(event_cookie);
	event_cookie = NULL;

	return hr;
}


/* -------------------------------------------------------------------------------------------------
OnEvent callback implementation: Main OnEvent handler called for events.
--------------------------------------------------------------------------------------------------*/

HRESULT DeviceEvent::OnEvent (IPortableDeviceValues* event_parameters)
{
	HRESULT hr = S_OK;

	if (event_parameters == NULL)
	{
		hr = E_POINTER;
	}

	// The event_parameters collection contains information about the event that was
	// fired. We'll at least need the EVENT_ID to figure out which event was fired
	// and based on that retrieve additional values from the collection.

	GUID   EventGUID;                // The ID as a struct.
	LPWSTR EventGUID_as_text = NULL;
	LPWSTR PnPDeviceID = NULL;
	
	// Display the ID of the object that was affected.
	// We can also obtain WPD_OBJECT_NAME, WPD_OBJECT_PERSISTENT_UNIQUE_ID, WPD_OBJECT_PARENT_ID, etc.
	LPWSTR ObjectID = NULL;
	LPWSTR ObjectDisplayName = NULL;
	LPWSTR ObjectFileName = NULL;
	LPWSTR ContainerFunctionalObjectID = NULL;

	event_parameters->GetGuidValue(WPD_EVENT_PARAMETER_EVENT_ID, &EventGUID);
	event_parameters->GetStringValue(WPD_EVENT_PARAMETER_PNP_DEVICE_ID, &PnPDeviceID);
	event_parameters->GetStringValue(WPD_OBJECT_ID, &ObjectID);
	event_parameters->GetStringValue(WPD_OBJECT_NAME, &ObjectDisplayName);
	event_parameters->GetStringValue(WPD_OBJECT_ORIGINAL_FILE_NAME, &ObjectFileName);
	event_parameters->GetStringValue(WPD_OBJECT_CONTAINER_FUNCTIONAL_OBJECT_ID, &ContainerFunctionalObjectID); // The object ID of the closest functional object containing this object.

	StringFromCLSID(EventGUID, &EventGUID_as_text);

	if (IsEqualGUID(WPD_EVENT_DEVICE_CAPABILITIES_UPDATED, EventGUID))
	{
		if (event_callback_device_capabilities_updated)
		{
			event_callback_device_capabilities_updated();
		}
		else
		{
			printf("Event WPD_EVENT_DEVICE_CAPABILITIES_UPDATED. No event callback found.\n");
		}
	}
	else if (IsEqualGUID(WPD_EVENT_DEVICE_REMOVED, EventGUID))
	{
		this->device->refresh();
		
		if (event_callback_device_removed)
		{
			event_callback_device_removed();
		}
		else
		{
			printf("Event WPD_EVENT_DEVICE_REMOVED. No event callback found.\n");
		}
	}
	else if (IsEqualGUID(WPD_EVENT_DEVICE_RESET, EventGUID))
	{
		if (event_callback_device_reset)
		{
			event_callback_device_reset();
		}
		else
		{
			printf("Event WPD_EVENT_DEVICE_RESET. No event callback found.\n");
		}
	}
	else if (IsEqualGUID(WPD_EVENT_OBJECT_ADDED, EventGUID))
	{
		this->device->refresh();

		if (event_callback_object_added)
		{
			event_callback_object_added();
		}
		else
		{
			printf("Event WPD_EVENT_OBJECT_ADDED. No event callback found.\n");
		}
	}
	else if (IsEqualGUID(WPD_EVENT_OBJECT_REMOVED, EventGUID))
	{
		this->device->refresh();
		
		if (event_callback_object_removed)
		{
			event_callback_object_removed();
		}
		else
		{
			printf("Event WPD_EVENT_OBJECT_REMOVED. No event callback found.\n");
		}
	}
	else if (IsEqualGUID(WPD_EVENT_OBJECT_TRANSFER_REQUESTED, EventGUID))
	{
		if (event_callback_object_transfer_requested)
		{
			event_callback_object_transfer_requested();
		}
		else
		{
			printf("Event WPD_EVENT_OBJECT_TRANSFER_REQUESTED. No event callback found.\n");
		}
	}
	else if (IsEqualGUID(WPD_EVENT_OBJECT_UPDATED, EventGUID))
	{
		this->device->refresh();

		if (event_callback_object_updated)
		{
			event_callback_object_updated();
		}
		else
		{
			printf("Event WPD_EVENT_OBJECT_UPDATED. No event callback found.\n");
		}
	}
	else if (IsEqualGUID(WPD_EVENT_SERVICE_METHOD_COMPLETE, EventGUID))
	{
		if (event_callback_service_method_complete)
		{
			event_callback_service_method_complete();
		}
		else
		{
			printf("Event WPD_EVENT_SERVICE_METHOD_COMPLETE. No event callback found.\n");
		}
	}
	else if (IsEqualGUID(WPD_EVENT_STORAGE_FORMAT, EventGUID))
	{
		if (event_callback_storage_format)
		{
			event_callback_storage_format();
		}
		else
		{
			printf("Event WPD_EVENT_STORAGE_FORMAT. No event callback found.\n");
		}
	}
	else
	{
		printf("Unknown Event: %ws\n", EventGUID_as_text);
	}

	if (ObjectID != NULL)
		printf("ObjectID                     : %ws\n", ObjectID);
	
	if (ObjectDisplayName != NULL)
		printf("WPD_OBJECT_NAME              : %ws\n", ObjectDisplayName);
	
	if (ObjectFileName != NULL)
		printf("WPD_OBJECT_ORIGINAL_FILE_NAME: %ws\n", ObjectFileName);

	if (ContainerFunctionalObjectID != NULL)
		printf("ContainerFunctionalObjectID  : %ws\n", ContainerFunctionalObjectID);

	
	if (EventGUID_as_text != NULL)
		CoTaskMemFree(EventGUID_as_text);
	
	if (ObjectID != NULL)
		CoTaskMemFree(ObjectID);
	
	if (PnPDeviceID != NULL)
		CoTaskMemFree(PnPDeviceID);
	
	if (ObjectDisplayName != NULL)
		CoTaskMemFree(ObjectDisplayName);
	
	if (ObjectFileName != NULL)
		CoTaskMemFree(ObjectFileName);
	
	if (ContainerFunctionalObjectID != NULL)
		CoTaskMemFree(ContainerFunctionalObjectID);

	// Note that we intentionally do not call Release on event_parameters since we don't own it.

	return hr;
}


void DeviceEvent::setEventCallback (DeviceEventType e, std::function<void()> callback)
{
	switch (e)
	{
		case DeviceEventType::WPD_EVENT_OBJECT_ADDED:
			event_callback_object_added = callback;
			break;

		case DeviceEventType::WPD_EVENT_OBJECT_REMOVED:
			event_callback_object_removed = callback;
			break;

		case DeviceEventType::WPD_EVENT_OBJECT_UPDATED:
			event_callback_object_updated = callback;
			break;

		case DeviceEventType::WPD_EVENT_DEVICE_RESET:
			event_callback_device_reset = callback;
			break;

		case DeviceEventType::WPD_EVENT_DEVICE_CAPABILITIES_UPDATED:
			event_callback_device_capabilities_updated = callback;
			break;

		case DeviceEventType::WPD_EVENT_DEVICE_REMOVED:
			
			event_callback_device_removed = callback;
			break;

		case DeviceEventType::WPD_EVENT_STORAGE_FORMAT:
			event_callback_storage_format = callback;
			break;

		default:
			printf("default!\n");
			break;
	}
}

} // namespace WPDLib
