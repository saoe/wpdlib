/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2019 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT

Interface of the class 'WPDLib::Storage'.

This class represents a functional object of/on the WPD device (WPD_FUNCTIONAL_CATEGORY_STORAGE).
*/

#ifndef WPDLIB_STORAGE_H
#define WPDLIB_STORAGE_H

#include <string>

#ifdef DLL_EXPORT
	#define DLL __declspec(dllexport)
#else
	#define DLL __declspec(dllimport)
#endif

namespace WPDLib
{

class DLL Storage
{
	public:
		Storage (IPortableDeviceProperties* const Properties, std::string ObjectID);
		
		std::string        getObjectID () const;
		std::string        getPersistentUniqueID () const;
		std::string        getName () const;
		std::string        getDescription () const;
		std::string        getFunctionalObjectID () const;
		std::string        getFilesystemType () const;
		std::string        getSerialNumber () const;
		unsigned long long getTotalCapacityInBytes () const;
		unsigned long long getTotalCapacityInObjects () const;
		unsigned long long getFreeBytes () const;
		unsigned long long getFreeObjects () const;
		unsigned long long getMaximumObjectSizeInBytes () const; // Maximum size of a single object (in bytes), that can be placed on this storage.
		unsigned long      getType () const;                     // As described by the enumeration WPD_STORAGE_TYPE_VALUES in Microsoft's PortableDevice.h.
		unsigned long      getAccess () const;
		
		void               updateFreeBytesCount ();

	private:
		IPortableDeviceProperties* DeviceProperties; // Pointer to the properties of the device, to which this storage belongs.
		
		std::string        ObjectID;
		std::string        PersistentUniqueID;
		std::string        Name;
		std::string        Description;
		std::string        FunctionalObjectID;
		std::string        FilesystemType;           // String description of the file system used by the storage,
		std::string        SerialNumber;             // A vendor-specific serial number for the storage.
		unsigned long long TotalCapacityInBytes;
		unsigned long long TotalCapacityInObjects;
		unsigned long long FreeCapacityInBytes;
		unsigned long long FreeCapacityInObjects;
		unsigned long long MaximumObjectSizeInBytes; // Maximum size of a single object (in bytes), that can be placed on this storage.
		unsigned long      Type;                     // As described by the enumeration WPD_STORAGE_TYPE_VALUES in Microsoft's PortableDevice.h.
		unsigned long      Access;

		void getStorageInfo (std::string ObjectID);
};

} // namespace WPDLib

#endif // WPDLIB_STORAGE_H