/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2015-2020 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT

Interface of the class 'WPDLib::Device'.

This class represents a physical WPD device, that is plugged in to the computer via USB.
For example a smartphone, that uses the Media Transfer Protocol (MTP),
or an USB flash drive, that acts like a Mass storage class (MSC) device.
*/

#ifndef WPDLIB_DEVICE_H
#define WPDLIB_DEVICE_H

#include "Object.h"
#include "Storage.h"
#include "Tree.h"
#include <Windows.h>
#include <PortableDevice.h>
#include <functional>
#include <map>
#include <vector>

#ifdef DLL_EXPORT
	#define DLL __declspec(dllexport)
#else
	#define DLL __declspec(dllimport)
#endif

class IPortableDevice;
class IPortableDeviceContent;
class IPortableDeviceProperties;
class IPortableDeviceValues;


namespace WPDLib
{

enum class ScanMode
{
	Basic,
	Full
};

class DeviceEvent;
enum class DeviceEventType;

class DLL Device
{
	public:
		Device (std::string pnp_device_id, IPortableDeviceValues* client_information);
		~Device ();
		
		void        getData (ScanMode sm = ScanMode::Full);
		bool        isMSCDevice () const;
		bool        isMTPDevice () const;
		std::string getID () const;
		void        getID (char* destination, size_t n) const;
		Storage     getStorage (int index);
		Storage     getStorage (std::string name);
		std::string getStorageName (int storage_index) const;
		void        getStorageName (int storage_index, char* destination, size_t n) const;
		int         getStorageCount () const;
		char        getDriveletter () const;
		HRESULT     enumerateContent (ScanMode sm = ScanMode::Full);
		void        openWithShell (std::string StorageName = "", std::string path = "");
		int         createFolder (std::string storage_name, std::string folder_path);
        int         createFolder (std::string folder_path);
        int         emptyFolder (std::string storage_name, std::string relative_folder_path, bool recursively = false);
        int         emptyFolder (std::string folder_path, bool recursively = false);
		int         copyFileToDevice (std::string file_path, std::string storage_name, std::string destination_folder, const GUID ContentType = WPD_CONTENT_TYPE_GENERIC_FILE);
		void        deleteObjects (std::string storage_name, std::vector<std::string> paths, bool recursively = false);
        int         deleteObject (std::string path, bool recursively = false);
        int         deleteObject (std::string storage_name, std::string path, bool recursively = false);
		HRESULT     renameObject (std::string storage_name, std::string relative_path_to_object, std::string new_name);
		bool        existsObject (std::string storage_name, std::string path);

		std::string getPrintableInfo ();

		std::string getFriendlyName () const;
		void        getFriendlyName (char* destination, size_t n) const;
		void        getManufacturer (char* destination, size_t n) const;
		void        getModel (char* destination, size_t n) const;
		void        getSerialNumber (char* destination, size_t n) const;
		void        getFirmwareVersion (char* destination, size_t n) const;
		void        getProtocol (char* destination, size_t n) const;
		void        getType (char* destination, size_t n) const;
		void        getPowerSource (char* destination, size_t n) const;
		void        getPowerLevel (char* destination, size_t n) const;

		void        refresh ();
		void        setEventCallback (DeviceEventType e, std::function<void()> callback);
		bool        supportsCommand (PROPERTYKEY command_key);

		Tree tree; // The content (file objects, etc.) of the device.

		void printTree (std::vector<Tree::Node> tn, int indentation_level = 0);

	private:
		IPortableDevice*           device;
		IPortableDeviceContent*    device_content;
		IPortableDeviceProperties* device_properties;
		DeviceEvent*               device_event;

		std::string PnPDeviceID;     // The Plug & Play Device ID; also known as "Device Instance Path" (in the Windows device manager).
		std::string Manufacturer;    // Human-readable device manufacturer name (based on WPD_DEVICE_MANUFACTURER).
		std::string Model;           // The device model (based on WPD_DEVICE_MODEL).
		std::string FriendlyName;    // Represents the friendly name set by the user on the device (based on WPD_DEVICE_FRIENDLY_NAME).
		std::string SerialNumber;    // The serial number of the device (based on WPD_DEVICE_SERIAL_NUMBER).
		std::string FirmwareVersion; // The firmware version for the device (based on WPD_DEVICE_FIRMWARE_VERSION).
		std::string Protocol;        // The device protocol that is being used (based on WPD_DEVICE_PROTOCOL).
		std::string SyncPartner;     // A human-readable description of a device's synchronization partner. This is a device, application, or server that the device communicates with to maintain a common state or group of files between both partners. Examples include e-mail programs and music libraries (based on WPD_DEVICE_SYNC_PARTNER).
		int         Type;            // As described by the enumeration WPD_DEVICE_TYPES in Microsoft's PortableDevice.h (based on WPD_DEVICE_TYPE).
		int         PowerSource;     // A WPD_POWER_SOURCES enumeration that indicates the power source of the device (based on WPD_DEVICE_POWER_SOURCE).
		int         PowerLevel;      // A value from 0 to 100 that specifies the power level of the device's battery, with 0 being none, and 100 being fully charged (based on WPD_DEVICE_POWER_LEVEL).
		
		std::vector<Storage>     storages;
		
		std::map<const GUID*, std::string> functional_categories;

		void        getBasicDeviceProperties ();
		void        getFunctionalObjects ();
		void        getStorageInfo (std::string FunctionalObjectID);
		std::string getStoragePUID (unsigned int index);
		std::string getStorageObjectIDByName (std::string name);
		Tree        getAllObjectsInFolder (std::string StorageObjectName, std::string PathToFolder, bool recursive = true);
		
		HRESULT enumerate (std::vector<Object>* output, std::string ObjectID, int max_level = 0, int cur_level = 0);
		HRESULT enumerate (std::vector<Tree::Node>* output, std::string object_id, int max_level = 0, int level = 0);
		
		std::string translateStorageType (unsigned long);
		std::string translateDeviceType (unsigned long) const;
		std::string getObjectID (std::string storage_name, std::string path);
		
		IPortableDeviceValues*  createObjectProperties (std::string ParentObjectID, std::string FilePath, IStream* FileStream);
		HRESULT                 StreamCopy (IStream* DestinationStream, IStream* SourceStream, DWORD TransferSize, DWORD* BytesTransfered);
};

} // namespace WPDLib

#endif
