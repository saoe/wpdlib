/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2015-2020 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT

Interface of the class 'WPDLib::DeviceEvent'.

This class registers for events that originate from a WPD-compatible device and does certain things
in the case that an event occurs (at the moment: print some info to the standard output...).
*/

#ifndef WPDLIB_DEVICEEVENT_H
#define WPDLIB_DEVICEEVENT_H

#include <cstdio>
#include <functional>
#include <Windows.h>
#include <PortableDeviceTypes.h> // Must be included before PortableDevice.h.
#include <PortableDevice.h>
#include <PortableDeviceApi.h>
#include "Device.h"

namespace WPDLib
{

enum class DeviceEventType
{
	WPD_EVENT_DEVICE_CAPABILITIES_UPDATED,
	WPD_EVENT_DEVICE_REMOVED,
	WPD_EVENT_DEVICE_RESET,
	WPD_EVENT_OBJECT_ADDED,
	WPD_EVENT_OBJECT_REMOVED,
	WPD_EVENT_OBJECT_TRANSFER_REQUESTED,
	WPD_EVENT_OBJECT_UPDATED,
	WPD_EVENT_SERVICE_METHOD_COMPLETE,
	WPD_EVENT_STORAGE_FORMAT
};

class DeviceEvent : public IPortableDeviceEventCallback
{
	public:
		DeviceEvent (Device* d);
		~DeviceEvent ();

		HRESULT __stdcall QueryInterface (REFIID riid, LPVOID* ppvObj);
		ULONG   __stdcall AddRef ();
		ULONG   __stdcall Release ();

		HRESULT __stdcall OnEvent (IPortableDeviceValues* event_parameters);    

		HRESULT __stdcall Register (IPortableDevice* device);
		HRESULT __stdcall Unregister (IPortableDevice* device);

		void setEventCallback (DeviceEventType e, std::function<void()> callback);

	private:
		ULONG  ref_count;    // Counter for the references to this object.
		LPWSTR event_cookie; // Cookie to use while unadvising

		Device* device;

		std::function<void()> event_callback_device_capabilities_updated;
		std::function<void()> event_callback_device_removed;
		std::function<void()> event_callback_device_reset;
		std::function<void()> event_callback_object_added;
		std::function<void()> event_callback_object_removed;
		std::function<void()> event_callback_object_transfer_requested;
		std::function<void()> event_callback_object_updated;
		std::function<void()> event_callback_service_method_complete;
		std::function<void()> event_callback_storage_format;
};

} // namespace WPDLib

#endif