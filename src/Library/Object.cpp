/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2016-2020 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT

Implementation of the class 'WPDLib::Object'.
*/

#define DLL_EXPORT

#include "Object.h"
#include "util.h"
#include <vector>
#include <iostream> // temp.
#include <PortableDeviceTypes.h> // Must be included before PortableDevice.h.
#include <PortableDevice.h>
#include <PortableDeviceApi.h>


namespace WPDLib
{

/* -------------------------------------------------------------------------------------------------
Constructor.
------------------------------------------------------------------------------------------------- */

Object::Object ()
{
	// Empty.
};


/* -------------------------------------------------------------------------------------------------
Constructor.
Collects data about the object (file, folder or other) specified by the argument 'object_id'.
------------------------------------------------------------------------------------------------- */

Object::Object (IPortableDeviceProperties* const Properties, std::string object_id)
{
	HRESULT hr = S_OK;
	
	// Specify the properties we are interested in.
	IPortableDeviceKeyCollection* PropertyKeys;

	hr = CoCreateInstance(CLSID_PortableDeviceKeyCollection, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceKeyCollection, (VOID**)&PropertyKeys);
	
	PropertyKeys->Add(WPD_OBJECT_ID);
	PropertyKeys->Add(WPD_OBJECT_PERSISTENT_UNIQUE_ID);
	PropertyKeys->Add(WPD_OBJECT_PARENT_ID);
	PropertyKeys->Add(WPD_OBJECT_CONTAINER_FUNCTIONAL_OBJECT_ID);
	PropertyKeys->Add(WPD_OBJECT_NAME);
	PropertyKeys->Add(WPD_OBJECT_ORIGINAL_FILE_NAME);
	PropertyKeys->Add(WPD_OBJECT_CONTENT_TYPE);
	PropertyKeys->Add(WPD_OBJECT_FORMAT);
	PropertyKeys->Add(WPD_OBJECT_SIZE);
    PropertyKeys->Add(WPD_OBJECT_DATE_CREATED);
    PropertyKeys->Add(WPD_OBJECT_DATE_AUTHORED);
    PropertyKeys->Add(WPD_OBJECT_DATE_MODIFIED);
	
	IPortableDeviceValues* PropertyValues;

    hr = Properties->GetValues(util::widen(object_id).c_str(), PropertyKeys, &PropertyValues);

    if (hr == S_FALSE)
    {
        hr = S_OK; /* GetValues() may return S_FALSE if one or more properties could not be retrieved; ignore it.
                      But for certain objects, we only need a few data points, like name etc.
                      If we would bail out here completely, we won't get that.                                    */
    }

    LPWSTR ObjectID = NULL;
    hr = PropertyValues->GetStringValue(WPD_OBJECT_ID, &ObjectID);
    if (hr == S_OK)
        this->ID = util::narrow(ObjectID);
    else
        return; /* Now we bail out: If we can't even get the ID, things will not improve from here on.
                   By the way: Played a bit with using exceptions (this being a c'tor), but that lead
                   to side effects elsewhere, which I didn't want to track down now.                   */

    LPWSTR UniqueID = NULL;
    hr = PropertyValues->GetStringValue(WPD_OBJECT_PERSISTENT_UNIQUE_ID, &UniqueID);
    if (hr == S_OK)
        this->PersistentUniqueID = util::narrow(UniqueID);

    LPWSTR ParentID = NULL;
    hr = PropertyValues->GetStringValue(WPD_OBJECT_PARENT_ID, &ParentID);
    if (hr == S_OK)
        this->ParentID = util::narrow(ParentID);

    LPWSTR ContFuncObjID = NULL;
    hr = PropertyValues->GetStringValue(WPD_OBJECT_CONTAINER_FUNCTIONAL_OBJECT_ID, &ContFuncObjID);
    if (hr == S_OK)
        this->ContainerFunctionalObjectID = util::narrow(ContFuncObjID);

    LPWSTR Name = NULL;
    hr = PropertyValues->GetStringValue(WPD_OBJECT_NAME, &Name);
    if (hr == S_OK)
        this->Name = util::narrow(Name);

    LPWSTR OriginalFileName = NULL;
    hr = PropertyValues->GetStringValue(WPD_OBJECT_ORIGINAL_FILE_NAME, &OriginalFileName);
    if (hr == S_OK)
        this->OriginalFileName = util::narrow(OriginalFileName);

    GUID ContentType = GUID_NULL;
    hr = PropertyValues->GetGuidValue(WPD_OBJECT_CONTENT_TYPE, &ContentType);
    if (hr == S_OK)
        this->ContentType = ContentType;

    GUID Format = GUID_NULL;
    hr = PropertyValues->GetGuidValue(WPD_OBJECT_FORMAT, &Format);
    if (hr == S_OK)
        this->Format = Format;

    unsigned long long Size = 0;
    hr = PropertyValues->GetUnsignedLargeIntegerValue(WPD_OBJECT_SIZE, &Size);
    if (hr == S_OK)
        this->SizeInBytes = Size;

    PROPVARIANT DateCreated;
    PropVariantInit(&DateCreated);
    hr = PropertyValues->GetValue(WPD_OBJECT_DATE_CREATED, &DateCreated);
    if (hr == S_OK)
    {
        if (DateCreated.vt == VT_DATE)
        {
            SYSTEMTIME system_time;

            if (VariantTimeToSystemTime(DateCreated.date, &system_time))
            {
                //// Converting UTC time to local time.
                //SystemTimeToFileTime(&utc_system_time, &file_time);
                //FileTimeToLocalFileTime(&file_time, &local_file_time);
                //FileTimeToSystemTime(&local_file_time, &local_system_time);

                std::ostringstream date;
                date << system_time.wYear << "-" << std::setfill('0') << std::setw(2) << system_time.wMonth << "-" << system_time.wDay <<
                 " " << system_time.wHour << ":" << system_time.wMinute << ":" << system_time.wSecond;
                this->DateCreated = date.str();
            }
        }
    }
    PropVariantClear(&DateCreated);

    PROPVARIANT DateAuthored;
    PropVariantInit(&DateAuthored);
    hr = PropertyValues->GetValue(WPD_OBJECT_DATE_AUTHORED, &DateAuthored);
    if (hr == S_OK)
    {
        if (DateAuthored.vt == VT_DATE)
        {
            SYSTEMTIME system_time;
                
            if (VariantTimeToSystemTime(DateAuthored.date, &system_time))
            {
                std::ostringstream date;
                date << system_time.wYear << "-" << std::setfill('0') << std::setw(2) << system_time.wMonth << "-" << system_time.wDay <<
                 " " << system_time.wHour << ":" << system_time.wMinute << ":" << system_time.wSecond;
                this->DateAuthored = date.str();
            }
        }
    }
    PropVariantClear(&DateAuthored);

    PROPVARIANT DateModified;
    PropVariantInit(&DateModified);
    hr = PropertyValues->GetValue(WPD_OBJECT_DATE_MODIFIED, &DateModified);
    if (hr == S_OK)
    {
        if (DateModified.vt == VT_DATE)
        {
            SYSTEMTIME system_time;
                
            if (VariantTimeToSystemTime(DateModified.date, &system_time))
            {
                std::ostringstream date;
                date << system_time.wYear << "-" << std::setfill('0') << std::setw(2) << system_time.wMonth << "-" << system_time.wDay <<
                 " " << system_time.wHour << ":" << system_time.wMinute << ":" << system_time.wSecond;
                this->DateModified = date.str();
            }
        }
    }
    PropVariantClear(&DateModified);
        
    if (hr == S_OK)
        this->RelativeStoragePath = buildRelativeStoragePath(Properties, object_id);

    if (PropertyKeys != NULL)
        PropertyKeys->Release();

    if (PropertyValues != NULL)
        PropertyValues->Release();
}


/*--------------------------------------------------------------------------------------------------
Checks whether the object is a folder or not.
--------------------------------------------------------------------------------------------------*/

bool Object::isFolder () const
{
	if (IsEqualGUID(this->ContentType, WPD_CONTENT_TYPE_FOLDER))
		return true;
	else
		return false;
}


/*--------------------------------------------------------------------------------------------------
Checks whether the object is a functional object or not.
--------------------------------------------------------------------------------------------------*/

bool Object::isFunctionalObject () const
{
	if (IsEqualGUID(this->ContentType, WPD_CONTENT_TYPE_FUNCTIONAL_OBJECT))
		return true;
	else
		return false;
}


/*--------------------------------------------------------------------------------------------------
Check if this object is of given content type (WPD_CONTENT_TYPE_*).

See PortableDevice.h or https://docs.microsoft.com/en-us/windows/win32/wpd_sdk/requirements-for-objects for full list.

Remarks:
- "WPD defines the following content types (as GUID values).
  A vendor is free to create their own custom-content type by providing their own GUID."
--------------------------------------------------------------------------------------------------*/

bool Object::isContentType (CLSID ct) const
{
	if (IsEqualGUID(this->ContentType, ct))
		return true;
	else
		return false;
}


/*--------------------------------------------------------------------------------------------------
Check if this object is of given format (WPD_OBJECT_FORMAT_*).

See PortableDevice.h or https://docs.microsoft.com/en-us/windows/win32/wpd_sdk/object-format-guids for full list.

Remarks:
- "If an MTP device supports vendor-specific formats, the device vendor can extend the
  WPD_OBJECT_FORMAT_UNSPECIFIED GUID with a custom format code of type UINT16."
- "A device service may optionally define additional format GUIDs.
  These are found in the header file for each device service."
--------------------------------------------------------------------------------------------------*/
bool Object::isFormat (CLSID f) const
{
	if (IsEqualGUID(this->Format, f))
		return true;
	else
		return false;
}


/* -------------------------------------------------------------------------------------------------
Returns the path (relative to the storage on the device) of an object.

So, just the 'Path...' part of "DEVICE\STORAGE\Path\To\A\Folder\Or\File.txt", without leading or trailing backslashes.
------------------------------------------------------------------------------------------------- */

std::string Object::buildRelativeStoragePath (IPortableDeviceProperties* properties, std::string ObjectID)
{
	std::vector<std::string> path_components; // Will contain the path segments in reverse hierarchical order.

	// -- Gather the names of the object's parents, i.e. building a folder hierarchy (excluding the device/storage root). --
	
	HRESULT                       hr = S_OK;
	IPortableDeviceKeyCollection* PropertyKeys;
	IPortableDeviceValues*        PropertyValues;
	
	// Specify the property we are interested in.
	hr = CoCreateInstance(CLSID_PortableDeviceKeyCollection, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceKeyCollection, (VOID**)&PropertyKeys);
	PropertyKeys->Add(WPD_OBJECT_PARENT_ID);
	
	std::string parent_id;
	std::string current_id = ObjectID;

	// Climb the parent folder hierarchy up and collect the names on the way.
	while (!current_id.empty())
	{
		if (hr == S_OK)
		{
			hr = properties->GetValues(util::widen(current_id).c_str(), PropertyKeys, &PropertyValues);

			if (hr == S_FALSE)
				hr = S_OK; // GetValues() may return S_FALSE if one or more properties could not be retrieved; ignore it.
		}

		// "Who's your daddy?"
		LPWSTR ParentID = NULL;
		if (hr == S_OK)
			hr = PropertyValues->GetStringValue(WPD_OBJECT_PARENT_ID, &ParentID);

		if (hr == S_OK)
			parent_id = util::narrow(ParentID);

		path_components.push_back(getOriginalFileName(properties, current_id));
		current_id = parent_id; // The root (= DEVICE) has no parent, thus at the end this will be empty.
	}

	if (PropertyKeys != NULL)
		PropertyKeys->Release();

	if (PropertyValues != NULL)
		PropertyValues->Release();

	// -- end of the segment --

	std::string path;

	for (std::vector<std::string>::reverse_iterator pos = path_components.rbegin(); pos != path_components.rend(); ++pos)
	{
		path.append(*pos);

		// Put in directory delimiters, but not at the end (after the final item; might be a file) and
		// not a the beginning (empty string; would result in a leading double-backslash).
		if ((pos != path_components.rend() - 1) && (!path.empty()))
			path.append("\\");
	}

	return path;
}


/* -------------------------------------------------------------------------------------------------
Gets the Original File Name of an object.
------------------------------------------------------------------------------------------------- */

std::string Object::getOriginalFileName (IPortableDeviceProperties* Properties, std::string ObjectID)
{
	HRESULT hr = S_OK;
	
	// Specify the properties we are interested in.
	IPortableDeviceKeyCollection* PropertyKeys;
	hr = CoCreateInstance(CLSID_PortableDeviceKeyCollection, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceKeyCollection, (VOID**)&PropertyKeys);
	PropertyKeys->Add(WPD_OBJECT_ORIGINAL_FILE_NAME);
	
	// Use the GetValues API to get the desired values:
	IPortableDeviceValues* PropertyValues;

	if (hr == S_OK)
	{
		hr = Properties->GetValues(util::widen(ObjectID).c_str(), PropertyKeys, &PropertyValues);

		if (hr == S_FALSE)
			hr = S_OK; // GetValues() may return S_FALSE if one or more properties could not be retrieved; ignore it.
	}

	// Get value(s).
	LPWSTR OriginalFileName = NULL;
	if (hr == S_OK)
		hr = PropertyValues->GetStringValue(WPD_OBJECT_ORIGINAL_FILE_NAME, &OriginalFileName);	

	// Clean up.
	if (PropertyKeys != NULL)
		PropertyKeys->Release();

	if (PropertyValues != NULL)
		PropertyValues->Release();

	// Return value.
	if (hr == S_OK)
		return util::narrow(OriginalFileName);
	else
		return "";
}


// TODO
std::string Object::getFullPath () const
{
	// Device.Name
	// + get...FunctionalObjectID).getName()
	// + this->RelativeStoragePath;
	return "";
}


std::string Object::getRelativeStoragePath () const
{
	return this->RelativeStoragePath;
}


std::string Object::getParentID () const
{
	return this->ParentID;
}


std::string Object::getID () const
{
	return this->ID;
}


std::string Object::getPersistentUniqueID () const
{
	return this->PersistentUniqueID;
}


std::string Object::getName () const
{
	return this->Name;
}


void Object::setName (std::string s)
{
	this->Name = s;
}


std::string Object::getOriginalFileName () const
{
	return this->OriginalFileName;
}


CLSID Object::getContentType () const
{
	return this->ContentType;
}


CLSID Object::getFormat () const
{
	return this->Format;
}


/* -------------------------------------------------------------------------------------------------
Helper function to get any kind of descriptive text for the object.
------------------------------------------------------------------------------------------------- */

std::string Object::getLabel () const
{
	if (this->OriginalFileName.empty())
		return this->Name;
	else
		return this->OriginalFileName;
}


// Simple getter functions for private member variable.

unsigned long long Object::getSizeInBytes () const
{
	return this->SizeInBytes;
}


std::string Object::getCreationTimeAsString () const
{
    return this->DateCreated;
}


std::string Object::getAuthorTimeAsString () const
{
    return this->DateAuthored;
}


std::string Object::getModificationTimeAsString () const
{
    return this->DateModified;
}

} // namespace WPDLib
