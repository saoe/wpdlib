/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2016, 2018-2021 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT

Miscellaneous helper functions.
*/

#define DLL_EXPORT

#include <sstream>
#include <string>
#include <vector>
#include <Windows.h>
#include "util.h"

namespace WPDLib
{

namespace util
{

/* -----------------------------------------------------------------------------
Narrows a wide string (conversion from UTF-16 to UTF-8).

[IMPORTANT] This code has been taken from my project/repository "The Nifty Oddity Toolkit.
            If you make any general/not project-specific modifications here, please also apply it there.
----------------------------------------------------------------------------- */

std::string narrow (const std::wstring& wstr)
{
	std::string str;
	int required_size = WideCharToMultiByte(CP_UTF8, 0, wstr.c_str(), -1, 0, 0, 0, 0);
	if(required_size > 0)
	{
		std::vector<char> buffer(required_size);
		WideCharToMultiByte(CP_UTF8, 0, wstr.c_str(), -1, &buffer[0], required_size, 0, 0);
		str.assign(buffer.begin(), buffer.end() - 1);
	}
	return str;
}


/* -----------------------------------------------------------------------------
Widens a narrow string (conversion from UTF-8 to UTF-16).

[IMPORTANT] This code has been taken from my project/repository "The Nifty Oddity Toolkit.
            If you make any general/not project-specific modifications here, please also apply it there.
----------------------------------------------------------------------------- */

std::wstring widen (const std::string& str)
{
	std::wstring wstr;
	int required_size = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, 0, 0);
	if(required_size > 0)
	{
		std::vector<wchar_t> buffer(required_size);
		MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, &buffer[0], required_size);
		wstr.assign(buffer.begin(), buffer.end() - 1);
	}
 	return wstr;
}


/* -------------------------------------------------------------------------------------------------
Splits a given path in a string into seperate chunks for a string vector.

Example:
std::string input = "Some\\Path\\String" -> std::vector<std::string> output: [0] - "Some"
                                                                             [1] - "Path"
                                                                             [3] - "String"
------------------------------------------------------------------------------------------------- */

std::vector<std::string> splitPath (std::string in)
{
	std::vector<std::string> out;
	std::string delimiter = "\\";
	size_t pos  =  0;
	size_t next = -1;

	do
	{
		pos = next + 1;
		next = in.find_first_of(delimiter, pos);
		out.push_back(in.substr(pos, next - pos));
	}
	while (next != std::string::npos);
	
	return out;
}


/*!
Splits a string in two values. The first shoul represent e.g. the storage ("Phone" or "F:")
and the second one the (relative) path, e.g. "Folder\\Subfolder\\File.ext".

\param full_path A string that contains the full path on a device to an object; e.g. "Card\\Folder\\File.ext".

\returns A tuple of two values: The first string is the name of the storage, the second string the rest of the path.
         On failure, both strings are empty.

*/

std::tuple<std::string, std::string> splitStorageAndPath (std::string full_path)
{
	std::vector<std::string> path_components = util::splitPath(full_path);

    if (!path_components.at(0).empty())
    {
        std::string relative_path;

        for (std::vector<std::string>::iterator p = path_components.begin() + 1; p != path_components.end(); ++p)
        {
            relative_path.append(*p).append("\\");
        }

        if (relative_path.back() == '\\')
            relative_path.pop_back(); // Delete trailing "\\".

        return std::make_tuple(path_components.at(0), relative_path);
    }
    else
    {
        return std::make_tuple("", "");
    }
}


/*--------------------------------------------------------------------------------------------------
Convert a Win32 GUID structure to a std::string.

UUID/GUID: https://en.wikipedia.org/wiki/Universally_unique_identifier
Win32 GUID struct: https://docs.microsoft.com/en-us/windows/win32/api/guiddef/ns-guiddef-guid
--------------------------------------------------------------------------------------------------*/

std::string GUIDToString (GUID* guid)
{
	char guid_string[37]; // 32 hex characters + 4 hyphens + null terminator
	snprintf(guid_string, sizeof(guid_string), "%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x",
	         guid->Data1, guid->Data2, guid->Data3,
	         guid->Data4[0], guid->Data4[1], guid->Data4[2], guid->Data4[3],
	         guid->Data4[4], guid->Data4[5], guid->Data4[6], guid->Data4[7]);
	return guid_string;
}


/*--------------------------------------------------------------------------------------------------
Convert a std::string (representing a normal GUID) to a Win32 GUID structure.

UUID/GUID: https://en.wikipedia.org/wiki/Universally_unique_identifier
Win32 GUID struct: https://docs.microsoft.com/en-us/windows/win32/api/guiddef/ns-guiddef-guid
--------------------------------------------------------------------------------------------------*/

GUID StringToGUID (const std::string& str)
{
	GUID guid;
	sscanf(str.c_str(), "{%8x-%4hx-%4hx-%2hhx%2hhx-%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx}",
	       &guid.Data1, &guid.Data2, &guid.Data3,
	       &guid.Data4[0], &guid.Data4[1], &guid.Data4[2], &guid.Data4[3],
	       &guid.Data4[4], &guid.Data4[5], &guid.Data4[6], &guid.Data4[7]);
	return guid;
}


/*--------------------------------------------------------------------------------------------------
Finds the first valid drive letter from a mask of drive letters.
The mask must be in the format bit 0 = A, bit 1 = B, bit 2 = C, and so on.
A valid drive letter is defined when the corresponding bit is set to 1.
Returns the first drive letter that was found.
--------------------------------------------------------------------------------------------------*/

char findFirstDriveFromMask (ULONG bitmask)
{
	char i;
	for (i = 0; i < 26; ++i)
	{
		if (bitmask & 0x1) { break; }
		bitmask = bitmask >> 1;
	}
	return (i + 'A');
}

} // namespace util

} // namespace WPDLib