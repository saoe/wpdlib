﻿/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2018-19 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT

Interface of the class 'WPDLib::Tree'.

A tree structure with nodes, that contain the objects of the device.
A WPD device does not use a classic filesystem; everything is an object, that represents either
a file, or a folder or a 'functional object' (such as a storage unit).
*/

#ifndef WPDLIB_TREE_H
#define WPDLIB_TREE_H

#include <vector>
#include "Object.h"

#ifdef DLL_EXPORT
	#define DLL __declspec(dllexport)
#else
	#define DLL __declspec(dllimport)
#endif


namespace WPDLib
{

class DLL Tree
{
	public:
		struct Node
		{
			Node (Node* parent = nullptr) {};
		
			/* Idea for later, maybe:
			enum Type {File, Folder};
			Type type;
			*/

			Node*             parent;
			Object            data;
			std::vector<Node> children;
		};

		std::vector<Node> nodes;

		int countChildren (std::vector<Node> n, int previous_count = 0) const;
};

} // namespace WPDLib

#endif