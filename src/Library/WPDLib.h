/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2015-2020 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT

Interface header file for application code that uses the WPDLib.
*/

#ifndef WPDLIB_H
#define WPDLIB_H

#include "DeviceManager.h"
#include "Device.h"
#include "DeviceEvent.h"
#include "Storage.h"
#include "util.h"

#endif
