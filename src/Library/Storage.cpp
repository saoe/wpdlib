/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2019 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT

Implementation of the class 'WPDLib::Storage'.
*/

#define DLL_EXPORT

#include <PortableDeviceTypes.h> // Must be included before PortableDevice.h.
#include <PortableDevice.h>
#include <PortableDeviceApi.h>
#include "Storage.h"
#include "util.h"

namespace WPDLib
{

/*--------------------------------------------------------------------------------------------------
Constructor.
--------------------------------------------------------------------------------------------------*/

Storage::Storage (IPortableDeviceProperties* const properties_of_the_device, std::string id_of_the_storage_object)
	: DeviceProperties(properties_of_the_device)
{
	getStorageInfo(id_of_the_storage_object);
}


/* -------------------------------------------------------------------------------------------------
Get basic info about this storage object of the device.
Inspired by https://github.com/kovidgoyal/calibre/blob/master/src/calibre/devices/mtp/windows/device_enumeration.cpp
------------------------------------------------------------------------------------------------- */

void Storage::getStorageInfo (std::string ObjectID_argument)
{
	IPortableDeviceKeyCollection* storage_properties;
	CoCreateInstance(CLSID_PortableDeviceKeyCollection, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceKeyCollection, (VOID**) &storage_properties);

	IPortableDeviceValues* values = 0;

	// Add the properties for retrieval.
	storage_properties->Add(WPD_OBJECT_ID);
	storage_properties->Add(WPD_OBJECT_PERSISTENT_UNIQUE_ID);
	storage_properties->Add(WPD_OBJECT_NAME);
	storage_properties->Add(WPD_STORAGE_DESCRIPTION);
	//storage_properties->Add(WPD_FUNCTIONAL_OBJECT_CATEGORY); // ?
	//storage_properties->Add(WPD_OBJECT_CONTENT_TYPE);        // ?
	storage_properties->Add(WPD_STORAGE_CAPACITY);
	storage_properties->Add(WPD_STORAGE_CAPACITY_IN_OBJECTS);
	storage_properties->Add(WPD_STORAGE_FREE_SPACE_IN_BYTES);
	storage_properties->Add(WPD_STORAGE_FREE_SPACE_IN_OBJECTS);
	storage_properties->Add(WPD_STORAGE_ACCESS_CAPABILITY);
	storage_properties->Add(WPD_STORAGE_FILE_SYSTEM_TYPE);
	storage_properties->Add(WPD_STORAGE_SERIAL_NUMBER);
	storage_properties->Add(WPD_STORAGE_MAX_OBJECT_SIZE);
	storage_properties->Add(WPD_STORAGE_TYPE);
	
	this->DeviceProperties->GetValues(util::widen(ObjectID_argument).c_str(), storage_properties, &values);

	this->ObjectID = "";
	this->PersistentUniqueID = "";
	this->Name = "";
	this->Description = "";
	this->Access = 0;
	this->FilesystemType = "";
	this->FreeCapacityInBytes = 0;
	this->FreeCapacityInObjects = 0;
	this->FunctionalObjectID = ObjectID_argument;
	this->MaximumObjectSizeInBytes = 0;
	this->SerialNumber = "";
	this->TotalCapacityInBytes = 0;
	this->TotalCapacityInObjects = 0;
	this->Type = 0;

	// --- Collect the data ---
	LPWSTR buffer_id = NULL;
	values->GetStringValue(WPD_OBJECT_ID, &buffer_id);
	if (buffer_id != 0)
		ObjectID = util::narrow(buffer_id);

	LPWSTR buffer_puid = NULL;
	values->GetStringValue(WPD_OBJECT_PERSISTENT_UNIQUE_ID, &buffer_puid);
	if (buffer_puid != 0)
		PersistentUniqueID = util::narrow(buffer_puid);

	LPWSTR buffer_name = NULL;
	values->GetStringValue(WPD_OBJECT_NAME, &buffer_name);
	if (buffer_name != 0)
		Name = util::narrow(buffer_name);

	LPWSTR buffer_desc = NULL;
	values->GetStringValue(WPD_STORAGE_DESCRIPTION, &buffer_desc);
	if (buffer_desc != 0)
		Description = util::narrow(buffer_desc);
					
	LPWSTR buffer_fs = NULL;
	values->GetStringValue(WPD_STORAGE_FILE_SYSTEM_TYPE, &buffer_fs);
	if (buffer_fs != 0)
		FilesystemType = util::narrow(buffer_fs);
					
	LPWSTR buffer_serial = NULL;
	values->GetStringValue(WPD_STORAGE_SERIAL_NUMBER, &buffer_serial);
	if (buffer_serial != 0)
		SerialNumber = util::narrow(buffer_serial);
					
	values->GetUnsignedLargeIntegerValue(WPD_STORAGE_CAPACITY, &TotalCapacityInBytes);
	values->GetUnsignedLargeIntegerValue(WPD_STORAGE_CAPACITY_IN_OBJECTS, &TotalCapacityInObjects);
	values->GetUnsignedLargeIntegerValue(WPD_STORAGE_FREE_SPACE_IN_BYTES, &FreeCapacityInBytes);
	values->GetUnsignedLargeIntegerValue(WPD_STORAGE_FREE_SPACE_IN_OBJECTS, &FreeCapacityInObjects);
	values->GetUnsignedLargeIntegerValue(WPD_STORAGE_MAX_OBJECT_SIZE, &MaximumObjectSizeInBytes);
	values->GetUnsignedIntegerValue(WPD_STORAGE_TYPE, &Type);
	values->GetUnsignedIntegerValue(WPD_STORAGE_ACCESS_CAPABILITY, &Access);

	// Cleaning up at the end...
	if (storage_properties != NULL)
		storage_properties->Release();
	
	if (values != NULL)
		values->Release();
}


std::string Storage::getObjectID () const
{
	return this->ObjectID;
}


std::string Storage::getPersistentUniqueID () const
{
	return this->PersistentUniqueID;
}


std::string Storage::getName () const
{
	return this->Name;
}


std::string Storage::getDescription () const
{
	return this->Description;
}


std::string Storage::getFunctionalObjectID () const
{
	return this->FunctionalObjectID;
}


std::string Storage::getFilesystemType () const
{
	return this->FilesystemType;
}


std::string Storage::getSerialNumber () const
{
	return this->SerialNumber;
}


/*--------------------------------------------------------------------------------------------------
Get the amount of total bytes on this storage unit.
--------------------------------------------------------------------------------------------------*/

unsigned long long Storage::getTotalCapacityInBytes () const
{
	return this->TotalCapacityInBytes;
}


unsigned long long Storage::getTotalCapacityInObjects () const
{
	return this->TotalCapacityInObjects;
}


/*--------------------------------------------------------------------------------------------------
Get the amount of free bytes on storage unit.
--------------------------------------------------------------------------------------------------*/

unsigned long long Storage::getFreeBytes () const
{
	return this->FreeCapacityInBytes;
}


unsigned long long Storage::getFreeObjects () const
{
	return this->FreeCapacityInObjects;
}


unsigned long long Storage::getMaximumObjectSizeInBytes () const
{
	return this->MaximumObjectSizeInBytes;
}


unsigned long Storage::getType () const
{
	return this->Type;
}


unsigned long Storage::getAccess () const
{
	return this->Access;
}


/*--------------------------------------------------------------------------------------------------
Update (refresh) the info/data/count of the amount of free bytes of this storage.

This is a separate method because the amount of free bytes will fluctuate most, compared with traits
like the name, serial number, etc. So a whole rescan of all device/storage properties would be overkill.
--------------------------------------------------------------------------------------------------*/

void Storage::updateFreeBytesCount ()
{
	IPortableDeviceKeyCollection* storage_properties;
	CoCreateInstance(CLSID_PortableDeviceKeyCollection, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceKeyCollection, (VOID**) &storage_properties);

	IPortableDeviceValues* values = 0;

	// Add the properties for retrieval.
	storage_properties->Add(WPD_STORAGE_CAPACITY);
	storage_properties->Add(WPD_STORAGE_FREE_SPACE_IN_BYTES);

	this->DeviceProperties->GetValues(util::widen(this->ObjectID).c_str(), storage_properties, &values);
	
	if (values != nullptr)
	{
		// Update the values in the currently processed storage_info.
		values->GetUnsignedLargeIntegerValue(WPD_STORAGE_CAPACITY, &this->TotalCapacityInBytes);
		values->GetUnsignedLargeIntegerValue(WPD_STORAGE_FREE_SPACE_IN_BYTES, &this->FreeCapacityInBytes);
	}

	if (storage_properties != NULL)
		storage_properties->Release();
	
	if (values != NULL)
		values->Release();
}

} // namespace WPDLib