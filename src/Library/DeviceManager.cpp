/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2015-2020 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT

Implementation of the class 'WPDLib::DeviceManager'.
*/

#define DLL_EXPORT

#include "DeviceManager.h"
#include "Device.h"
#include "version.h"
#include "util.h"

#include <Windows.h>
#include <dbt.h>

#include <PortableDeviceTypes.h> // Must be included before PortableDevice.h.
#include <PortableDevice.h>
#include <PortableDeviceApi.h>

// Load GUID Classes (usually located in a header file of the Windows Driver Kit (WDK)).
static GUID GUID_DEVINTERFACE_USB_HUB             = {0xF18A0E88, 0xC30C, 0x11D0, {0x88, 0x15, 0x00, 0xA0, 0xC9, 0x06, 0xBE, 0xD8}};
static GUID GUID_DEVINTERFACE_USB_DEVICE          = {0xA5DCBF10, 0x6530, 0x11D2, {0x90, 0x1F, 0x00, 0xC0, 0x4F, 0xB9, 0x51, 0xED}};
static GUID GUID_DEVINTERFACE_USB_HOST_CONTROLLER = {0x3ABF6F2D, 0x71C4, 0x462A, {0x8A, 0x92, 0x1E, 0x68, 0x61, 0xE6, 0xAF, 0x27}};

namespace WPDLib
{

/* Constructor */

DeviceManager::DeviceManager (HWND hwnd)
{
	// Instantiate an IPortableDeviceManager. (Instance is saved, for future need, in a class member.)
	ipdm = NULL;
	CoCreateInstance(CLSID_PortableDeviceManager, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceManager, (void**) &ipdm);
	
	// Identify us (this program/the caller) with information about the inquiring application.
	// (The object instance is saved, for future need, in a class member.)
	client_information = NULL;
	CoCreateInstance(CLSID_PortableDeviceValues, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceValues, (void**) &client_information);
	(*&client_information)->SetStringValue(WPD_CLIENT_NAME, PROJECT_NAME);
	(*&client_information)->SetUnsignedIntegerValue(WPD_CLIENT_MAJOR_VERSION, VERSION_MAJOR);
	(*&client_information)->SetUnsignedIntegerValue(WPD_CLIENT_MINOR_VERSION, VERSION_MINOR);
	(*&client_information)->SetUnsignedIntegerValue(WPD_CLIENT_REVISION, VERSION_REVISION);
	(*&client_information)->SetUnsignedIntegerValue(WPD_CLIENT_SECURITY_QUALITY_OF_SERVICE, SECURITY_IMPERSONATION);
	
	registerDeviceNotifications(hwnd);

	this->scan(ScanMode::Basic); // Initial scan for device data.
}


/* Destructor */

DeviceManager::~DeviceManager ()
{
	unregisterDeviceNotifications();

	//ipdm->Release(); // also as (*&ipdm)->... Causes (indirectly) a crash...? When is the destructor called?
}


/* Set up which kind of device notification messages are interesting for us and register device
notification with the window that will receive the broadcast messages when devices arrive/leave. */

void DeviceManager::registerDeviceNotifications (HWND hwnd)
{
	HANDLE* handle = (HANDLE*)hwnd;

	// ---- For USB ----
	device_notification_handle_usb = NULL;
	DEV_BROADCAST_DEVICEINTERFACE NotificationFilterUSB;
	ZeroMemory (&NotificationFilterUSB, sizeof(NotificationFilterUSB));
	NotificationFilterUSB.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
	NotificationFilterUSB.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
	NotificationFilterUSB.dbcc_classguid = GUID_DEVINTERFACE_USB_DEVICE;
	device_notification_handle_usb = RegisterDeviceNotification(handle, &NotificationFilterUSB, DEVICE_NOTIFY_WINDOW_HANDLE);

	// ---- For WPD (not even sure if that is really neccessary, didn't notice any changes/effects...) ----
	device_notification_handle_wpd = NULL;
	DEV_BROADCAST_DEVICEINTERFACE NotificationFilterWPD;
	ZeroMemory (&NotificationFilterWPD, sizeof(NotificationFilterWPD));
	NotificationFilterWPD.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
	NotificationFilterWPD.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
	NotificationFilterWPD.dbcc_classguid = GUID_DEVINTERFACE_WPD;
	device_notification_handle_wpd = RegisterDeviceNotification(handle, &NotificationFilterWPD, DEVICE_NOTIFY_WINDOW_HANDLE);
}


/* Unregister device notification messages again. */

void DeviceManager::unregisterDeviceNotifications ()
{
	if (NULL != device_notification_handle_usb)
		UnregisterDeviceNotification(device_notification_handle_usb);

	if (NULL != device_notification_handle_wpd)
		UnregisterDeviceNotification(device_notification_handle_wpd);
}


/* Get the total number of devices found on the system. */

int DeviceManager::countDevices () const
{
	int n = 0;

	HRESULT hr = (*&ipdm)->RefreshDeviceList();
	
	if (SUCCEEDED(hr))
	{
		(*&ipdm)->GetDevices(NULL, (DWORD*)&n);
	}

	return n;
}


/* Get information on the WPD-compatible devices that are currently plugged in. */

void DeviceManager::scan (ScanMode sm)
{
	DWORD c = 0;
	clearDevicesList();
	
	HRESULT hr = (*&ipdm)->RefreshDeviceList();
	
	if (SUCCEEDED(hr))
	{
		hr = (*&ipdm)->GetDevices(NULL, &c);    // Get initial count of connected devices.

		if (SUCCEEDED(hr) && (c > 0))
		{
			// Get the "PnP [Plug and Play] Device IDs" (or "Device Instance Path", in the lingo of the Windows device manager's property sheet).
			PWSTR* device_list = new PWSTR[c];

			if (device_list != nullptr)
			{
				(*&ipdm)->GetDevices(device_list, &c);

				for (unsigned int i = 0; i < c; i++)
				{
					Device* device = new Device(util::narrow(device_list[i]), client_information);
					device->getData(sm);
					addDeviceToList(device);

					CoTaskMemFree(device_list[i]);
					device_list[i] = nullptr;
				}

				delete[] device_list;
				device_list = nullptr;
			}
		}
	}
}


/* Add a device to the internal container. */

void DeviceManager::addDeviceToList (Device* d)
{
	devices.push_back(d);
}


/* Remove a device from the internal container. */

void DeviceManager::removeDeviceFromList (std::string FriendlyName)
{
	for (std::vector<Device*>::iterator pos = devices.begin(); pos != devices.end(); /* Do not increment here; could step beyond .end()! */)
	{
		if ((*pos)->getFriendlyName() == FriendlyName)
		{
			delete *pos;
			pos = devices.erase(pos); // erase() returns the next valid iterator (or .end()).
		}
		else
		{
			++pos; // Skip iterator to the next position.
		}
	}
}


/* Erase the whole container of devices and frees the memory. */

void DeviceManager::clearDevicesList ()
{
	for (std::vector<Device*>::iterator pos = devices.begin(); pos != devices.end(); /* Do not increment here; could step beyond .end()! */)
	{
		if (*pos)
		{
			delete *pos;
			pos = devices.erase(pos++);
		}
	}
}


/* Return the pointer to a Device object (from the collection/container) that matches the search parameter.

Overloaded method: Uses an integer index into the 'devices' vector.
Note that at() will throw an out_of_range std::exception if index is not within bounds. */

Device* DeviceManager::getDevice (int index)
{
	return devices.at(index);
}


/* Return the pointer to a Device object (from the collection/container) that matches the search parameter.

Overloaded method: Uses a (case sensitive!) string to compare against the device's Friendly Name. */

Device* DeviceManager::getDevice (std::string FriendlyName)
{
	for (std::vector<Device*>::iterator pos = devices.begin(); pos != devices.end(); ++pos)
	{
		if (FriendlyName == (*pos)->getFriendlyName())
			return *pos;
	}
	return nullptr;
}


/* Relay the named function to handle the named event type of the device. */

void DeviceManager::setEventCallback (DeviceEventType e, std::function<void()> callback)
{
	for (std::vector<Device*>::iterator pos = devices.begin(); pos != devices.end(); ++pos)
	{
		(*pos)->setEventCallback(e, callback);

	}
}

} // namespace WPDLib
