/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2015-2020 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT

Implementation of the class 'WPDLib::Device'.
*/

#define DLL_EXPORT

#include "Device.h"
#include "DeviceEvent.h"
#include "util.h"
#include <iostream>
#include <string>
#include <shlwapi.h>
#include <tuple>

namespace WPDLib
{

/* -------------------------------------------------------------------------------------------------
Constructor
------------------------------------------------------------------------------------------------- */

Device::Device (std::string pnp_device_id, IPortableDeviceValues* client_information) : device(nullptr)
                                                                                      , device_content(nullptr)
                                                                                      , device_properties(nullptr)
                                                                                      , device_event(nullptr)
{
	this->PnPDeviceID = pnp_device_id;
	
	HRESULT hr = CoCreateInstance(CLSID_PortableDevice, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDevice, (VOID**) &device);

	if (SUCCEEDED(hr))
	{
		hr = device->Open(util::widen(this->PnPDeviceID).c_str(), client_information);

		if (SUCCEEDED(hr))
		{
			// Register for device event notifications from the system.
			this->device_event = new DeviceEvent(this);
			if (this->device_event != nullptr)
				this->device_event->Register(device);

			// Gathering basic information for this device: Content and properties.
			hr = this->device->Content(&device_content);
			if (SUCCEEDED(hr))
				this->device_content->Properties(&device_properties);
		}
	}
}


/* -------------------------------------------------------------------------------------------------
Destructor
------------------------------------------------------------------------------------------------- */

Device::~Device ()
{
	this->device_properties->Release();
	//this->device_content->Release();
		// Throws an exception (access violation) on plug on/off! My COM understandig is not great; maybe it has been released before? Didn't find any.
	this->device_event->Unregister(device);
	device->Release();
}


/* -------------------------------------------------------------------------------------------------
Get basic data of the device.
------------------------------------------------------------------------------------------------- */

void Device::getData (ScanMode sm)
{
	getBasicDeviceProperties();
	getFunctionalObjects();
	enumerateContent(sm);
}


/* -------------------------------------------------------------------------------------------------
Get the basic properties of the device, like model, name, serial number, etc.
------------------------------------------------------------------------------------------------- */

void Device::getBasicDeviceProperties ()
{
	IPortableDeviceKeyCollection* property_keys;
	IPortableDeviceValues*        property_values;

	CoCreateInstance(CLSID_PortableDeviceKeyCollection, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceKeyCollection, (VOID**)&property_keys);

	// Specify the properties that we want to retrieve:
	property_keys->Add(WPD_DEVICE_MANUFACTURER);
	property_keys->Add(WPD_DEVICE_MODEL);
	property_keys->Add(WPD_DEVICE_FRIENDLY_NAME);
	property_keys->Add(WPD_DEVICE_SERIAL_NUMBER);
	property_keys->Add(WPD_DEVICE_FIRMWARE_VERSION);
	property_keys->Add(WPD_DEVICE_PROTOCOL);
	property_keys->Add(WPD_DEVICE_SYNC_PARTNER);
	property_keys->Add(WPD_DEVICE_TYPE);
	property_keys->Add(WPD_DEVICE_POWER_SOURCE);
	property_keys->Add(WPD_DEVICE_POWER_LEVEL);

	// Get the value of the desired properties:
	this->device_properties->GetValues(WPD_DEVICE_OBJECT_ID, property_keys, &property_values);
	LPWSTR buffer = 0;
	
	property_values->GetStringValue(WPD_DEVICE_MANUFACTURER, &buffer);
	this->Manufacturer = util::narrow(buffer);
	property_values->GetStringValue(WPD_DEVICE_MODEL, &buffer);
	this->Model = util::narrow(buffer);
	property_values->GetStringValue(WPD_DEVICE_FRIENDLY_NAME, &buffer);
	this->FriendlyName = util::narrow(buffer);
	if (this->FriendlyName.empty()) { this->FriendlyName = this->Model; }
	property_values->GetStringValue(WPD_DEVICE_SERIAL_NUMBER, &buffer);
	this->SerialNumber = util::narrow(buffer);
	property_values->GetStringValue(WPD_DEVICE_FIRMWARE_VERSION, &buffer);
	this->FirmwareVersion = util::narrow(buffer);
	property_values->GetStringValue(WPD_DEVICE_PROTOCOL, &buffer);
	this->Protocol = util::narrow(buffer);
	property_values->GetStringValue(WPD_DEVICE_SYNC_PARTNER, &buffer);
	this->SyncPartner = util::narrow(buffer);
	property_values->GetUnsignedIntegerValue(WPD_DEVICE_TYPE, (ULONG*)&this->Type);
	property_values->GetUnsignedIntegerValue(WPD_DEVICE_POWER_SOURCE, (ULONG*)&this->PowerSource);
	property_values->GetUnsignedIntegerValue(WPD_DEVICE_POWER_LEVEL, (ULONG*)&this->PowerLevel);
	
	property_values->Release();
	property_keys->Release();
}


/* -------------------------------------------------------------------------------------------------
List all functional objects on the device
------------------------------------------------------------------------------------------------- */

void Device::getFunctionalObjects ()
{
	HRESULT hr = S_OK;
    IPortableDeviceCapabilities*          capabilities;
    IPortableDevicePropVariantCollection* categories;
    DWORD number_of_categories = 0;

    if (this->device == NULL)
        return;

    // Get the capabilities-specific methods and the functional categories supported by the device.
    // We will use these categories to enumerate functional objects that fall within them.

	hr = this->device->Capabilities(&capabilities);
	if (FAILED(hr))
		return;
    
	hr = capabilities->GetFunctionalCategories(&categories);
	if (FAILED(hr))
		return;

	hr = categories->GetCount(&number_of_categories);
	if (FAILED(hr))
		return;

    // Loop through the functional categories and get the list of functional object identifiers for each.
	for (DWORD i = 0; i < number_of_categories; i++)
	{
		PROPVARIANT pv = {0};
		PropVariantInit(&pv);
		
		hr = categories->GetAt(i, &pv);
		
		if (SUCCEEDED(hr))
		{
			if ((pv.puuid != NULL) && (pv.vt == VT_CLSID))
			{
				REFGUID guidCategory = *pv.puuid;
				std::string category_string;

				if (IsEqualGUID(WPD_FUNCTIONAL_CATEGORY_STORAGE, guidCategory))
					category_string = "Storage";
				else if (IsEqualGUID(WPD_FUNCTIONAL_CATEGORY_STILL_IMAGE_CAPTURE, guidCategory))
					category_string = "Still Image Capture";
				else if (IsEqualGUID(WPD_FUNCTIONAL_CATEGORY_AUDIO_CAPTURE, guidCategory))
					category_string = "Audio Capture";
				else if (IsEqualGUID(WPD_FUNCTIONAL_CATEGORY_SMS, guidCategory))
					category_string = "SMS";
				else if (IsEqualGUID(WPD_FUNCTIONAL_CATEGORY_RENDERING_INFORMATION, guidCategory))
					category_string = "Rendering Information";
				else
					category_string = "Unknown";

				//functional_categories.insert(std::pair<const GUID*, std::string>(&guidCategory, stringCategory));
				//std::cout << "Functional Category: " << category_string << std::endl;

				// Display the object identifiers for all functional objects within this category.
				IPortableDevicePropVariantCollection* FunctionalObjectIDs;
				hr = capabilities->GetFunctionalObjects(*pv.puuid, &FunctionalObjectIDs);
				
				if (SUCCEEDED(hr))
				{
					// Display all Functional Object Identifiers contained in an IPortableDevicePropVariantCollection.
					PROPVARIANT foid_pv = {0};
					PropVariantInit(&foid_pv);
					
					DWORD number_of_functional_objects = 0;
					HRESULT hr = FunctionalObjectIDs->GetCount(&number_of_functional_objects);

					std::string foid_string;
					
					if (SUCCEEDED(hr))
					{
						for (DWORD j = 0; j < number_of_functional_objects; j++)
						{
							hr = FunctionalObjectIDs->GetAt(j, &foid_pv);
							if (SUCCEEDED(hr))
							{
								if ((foid_pv.pwszVal != NULL) && (foid_pv.vt == VT_LPWSTR))
								{
									//printf("(%d) %ws\n", j+1, foid_pv.pwszVal);
									foid_string = util::narrow(foid_pv.pwszVal);
								}
							}

							PropVariantClear(&foid_pv);
						}

						// Storage node found.
						if (IsEqualGUID(WPD_FUNCTIONAL_CATEGORY_STORAGE, *pv.puuid))
						{
							// Outside the loop, because this function loops itself over child objects.
							getStorageInfo(foid_string);
						}
					}
				}
				else
				{
					printf("Error: Failed to get functional objects (error code 0x%lx).\n", hr);
				}
			}
			else
			{
				printf("Error: Invalid functional category found!\n");
			}
		}

		PropVariantClear(&pv);
	}

	capabilities->Release();
	categories->Release();
}


/*--------------------------------------------------------------------------------------------------
Get basic storage unit infos.

Loops over all objects of the device, and if one of it turns out to be a functional object of the
category "storage", then a new WPDLib::Storage object will be created.
That will contain the data/info of the detected storage unit and will be added to the device's
collection of storage units.
--------------------------------------------------------------------------------------------------*/

void Device::getStorageInfo (std::string FunctionalObjectID)
{
	IPortableDeviceKeyCollection* storage_properties;
	CoCreateInstance(CLSID_PortableDeviceKeyCollection, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceKeyCollection, (VOID**) &storage_properties);

	IPortableDeviceValues* values = 0;
	
	// Enumerate children (if any) of provided object
	IEnumPortableDeviceObjectIDs* objects = 0;
	this->device_content->EnumObjects(0, WPD_DEVICE_OBJECT_ID, NULL, &objects);

	HRESULT hr = S_OK;
	HRESULT hr2;
	DWORD   fetched = 0;
	LPWSTR  object_id;
	GUID    guid;

	// Add the properties for retrieval.
	storage_properties->Add(WPD_FUNCTIONAL_OBJECT_CATEGORY);
	storage_properties->Add(WPD_OBJECT_CONTENT_TYPE);

	while (hr == S_OK)
	{
		hr = objects->Next(1, &object_id, &fetched);

		if (SUCCEEDED(hr))
		{
			hr2 = this->device_properties->GetValues(object_id, storage_properties, &values);

			if SUCCEEDED(hr2)
			{
				if (SUCCEEDED(values->GetGuidValue(WPD_OBJECT_CONTENT_TYPE, &guid))
					&& IsEqualGUID(guid, WPD_CONTENT_TYPE_FUNCTIONAL_OBJECT)
					&& SUCCEEDED(values->GetGuidValue(WPD_FUNCTIONAL_OBJECT_CATEGORY, &guid))
					&& IsEqualGUID(guid, WPD_FUNCTIONAL_CATEGORY_STORAGE))
				{
					Storage s(this->device_properties, util::narrow(object_id));
					storages.push_back(s);
				}
			}

			CoTaskMemFree(object_id);
			object_id = 0;
		}
	}

	if (objects != NULL) objects->Release();
	if (storage_properties != NULL) storage_properties->Release();
	if (values != NULL) values->Release();
}


/* -------------------------------------------------------------------------------------------------
Translate the values of the WPD_STORAGE_TYPE property to text string.
------------------------------------------------------------------------------------------------- */

std::string Device::translateStorageType (unsigned long value)
{
	switch (value)
	{
		case WPD_STORAGE_TYPE_UNDEFINED:
			return "Undefined";
  
		case WPD_STORAGE_TYPE_FIXED_ROM:
			return "Fixed ROM";
  
		case WPD_STORAGE_TYPE_REMOVABLE_ROM:
			return "Removable ROM";
  
		case WPD_STORAGE_TYPE_FIXED_RAM:
			return "Fixed RAM";
  
		case WPD_STORAGE_TYPE_REMOVABLE_RAM:
			return "Removable RAM";
		
		default:
			return "(Unknown Storage Type)";
	}
}


/* -------------------------------------------------------------------------------------------------
Translate the values of the WPD_DEVICE_TYPE property to text string.
------------------------------------------------------------------------------------------------- */

std::string Device::translateDeviceType (unsigned long value) const
{
	switch (value)
	{
		case WPD_DEVICE_TYPE_GENERIC:
			return "Generic";

		case WPD_DEVICE_TYPE_CAMERA:
			return "Camera";

		case WPD_DEVICE_TYPE_MEDIA_PLAYER:
			return "Media Player";

		case WPD_DEVICE_TYPE_PHONE:
			return "Phone";

		case WPD_DEVICE_TYPE_VIDEO:
			return "Video";

		case WPD_DEVICE_TYPE_PERSONAL_INFORMATION_MANAGER:
			return "Personal Information Manager";
		
		case WPD_DEVICE_TYPE_AUDIO_RECORDER:
			return "Audio Recorder";

		default:
			return "Unknown Device Type";
	}
}


/* -------------------------------------------------------------------------------------------------
Text string with info about the device.
------------------------------------------------------------------------------------------------- */

std::string Device::getPrintableInfo ()
{
	std::string general = std::string("--- General Info ------------------------------\n")\
	+ "Manufacturer   : " + this->Manufacturer                                + "\n"\
	+ "Model          : " + this->Model                                       + "\n"\
	+ "FriendlyName   : " + this->FriendlyName                                + "\n"\
	+ "SerialNumber   : " + this->SerialNumber                                + "\n"\
	+ "FirmwareVersion: " + this->FirmwareVersion                             + "\n"\
	+ "Protocol       : " + this->Protocol                                    + "\n"\
	+ "Type           : " + translateDeviceType(this->Type)                   + "\n"\
	+ "PowerSource    : " + (this->PowerSource == 0 ? "Battery" : "External") + "\n"\
	+ "PowerLevel     : " + util::NumberToString(this->PowerLevel)            + "\n"\
	+ "PnPDeviceID    : " + this->PnPDeviceID                                 + "\n"\
	+ "\n";

	std::string storage = "";
	
	for (std::vector<Storage>::iterator pos = storages.begin(); pos != storages.end(); pos++)
	{
		storage.append(std::string("    --- Storage Info ---\n")\
		+ "    ID                          : " + pos->getObjectID()                                             + "\n"\
		+ "    PersistentUniqueID          : " + pos->getPersistentUniqueID()                             + "\n"\
		+ "    Name                        : " + pos->getName()                                           + "\n"\
		+ "    Storage description         : " + pos->getDescription()                                    + "\n"\
		+ "    Functional Object ID        : " + pos->getFunctionalObjectID()                             + "\n"\
		+ "    Storage file-system type    : " + pos->getFilesystemType()                                 + "\n"\
		+ "    Storage serial-number       : " + pos->getSerialNumber()                                   + "\n"\
		+ "    Storage type                : " + translateStorageType(pos->getType())                     + "\n"\
		+ "    Total byte capacity         : " + util::translateBytesToHandySizeText(pos->getTotalCapacityInBytes()) + " (" + util::NumberToString(pos->getTotalCapacityInBytes()) + " Bytes)\n"\
		+ "    Free byte capacity          : " + util::translateBytesToHandySizeText(pos->getFreeBytes()) + " (" + util::NumberToString(pos->getFreeBytes()) + " Bytes)\n"\
		+ "    Total object capacity       : " + util::NumberToString(pos->getTotalCapacityInObjects())   + "\n"\
		+ "    Free object capacity        : " + util::NumberToString(pos->getFreeObjects())              + "\n"\
		+ "    Maximum object size in bytes: " + util::NumberToString(pos->getMaximumObjectSizeInBytes()) + "\n"\
		+ "    Access capability           : " + util::NumberToString(pos->getAccess())                   + "\n"\
		+ "\n");
	}
	
	return general + storage;
}


/* -------------------------------------------------------------------------------------------------
Opens a given path with the shell.

If no argument for 'path' is given, the storage overview will be opened.
If no argument for 'storage_name' is given, all found storage units will be shown.

The Windows file explorer for example displays "My Computer\Sascha Offe (GT-I9195)\Phone\Pictures\Screenshots".
This is composed as:
  ::{20D04FE0-3AEA-1069-A2D8-08002B30309D}\                                                 -> Shell namespace ID (CLSID) for 'My Computer'.
+ \\\?\USB#VID_04E8&PID_6860&MI_00#7&3826ceb&0&0000#{6ac27878-a6fa-4155-ba85-f98f491d4f33}\ -> Plug'n'Play Device ID.
+ SID-{10001,SECZ9519043CHOHB,5793759232}\ (... or simply use the name, e.g. 'Phone')       -> Storage's Persistent Unique ID (or name).
+ ...\                                                                                      -> Folder object name.
+ ...                                                                                       -> File object name.

Note: Testing this up after moving to Windows 10, it now seems only to work when the storage's name is supplied, but not with the storage's PUID.
      Bug or feature (by Microsoft)?

	  And another case of "WPD is fragile and fickle": Suddenly, this command could open a folder (file explorer),
	  but not a file (default app, regardless of the type).
	  Plugged the smartphone out and in again, and then it worked again, also with a file -- odd behaviour.

[TODO] Currently, no leading backlash is allowed in the subdirectory argument:
       Good: "Music\\2004 - The Album X"
       Bad:  "\\Music\\2004 - The Album X"
	   -> Remove leading and trailing (back)slashes from 'path' argument.
------------------------------------------------------------------------------------------------- */

void Device::openWithShell (std::string StorageName, std::string path)
{
	/* Composing the base path: "My Computer"-CLSID, plus separator ("\", escaped), plus Device Instance ID. */
	std::string p("::{20D04FE0-3AEA-1069-A2D8-08002B30309D}\\" + this->PnPDeviceID);
	
	if (!StorageName.empty())
	{
		/* Add to the base path the Persistent Unique ID (or name) of the storage object; note that
		   some devices have multiple storages: Internal (e.g. 'Phone') and external (e.g. a SD card).
		   The Persistent Unique ID of the storage is built like this for example:
		   "SID-{10001,SECZ9519043CHOHB,5793759232}", while the name of the storage is simply
		   something like "Phone". */
		p.append("\\").append(StorageName);

		if (!path.empty())
		{
			/* Finally, the name of/path to the directory or file object is appended. */
			p.append("\\").append(path);
		}
	}
		
	/* Previously, I wanted to check here whether the path is a file or a directory, but that doesn't work that easily
		(and is actually not necessary).
		- PathFileExists() doesn't like the UNC format of the path (i.e. "::{...}\\\?\...")
		- GetFileAttributes() & FILE_ATTRIBUTE_DIRECTORY return false values, probably because the path doesn't lead
		  to a native/ordinary Windows filesystem object.
		So we just call the shell instead and let it do the right thing (directory: open file explorer window; file: run default application). */
	ShellExecuteW(NULL, NULL, util::widen(p).c_str(), 0, 0, SW_SHOWNORMAL);
}


/* -------------------------------------------------------------------------------------------------
Tells whether this device is using the protocol for Mass Storage Class (MSC) devices or not.
Asking a WPD device for WPD_DEVICE_PROTOCOL gives back (based on test results) a string like "MSC: " or "MTP: 1.00".
This method depends on the existence of a class member that already contains such a value; it only analyzes that string.
------------------------------------------------------------------------------------------------- */

bool Device::isMSCDevice () const
{
	std::size_t found = this->Protocol.find("MSC");

	if (found == std::string::npos)
		return false;
	else
		return true;
}


/* -------------------------------------------------------------------------------------------------
Tells whether this device is using the protocol for Media Transfer Protocol (MTP) devices or not.
Asking a WPD device for WPD_DEVICE_PROTOCOL gives back (based on test results) a string like "MSC: " or "MTP: 1.00".
This method depends on the existence of a class member that already contains such a value; it only analyzes that string.
------------------------------------------------------------------------------------------------- */

bool Device::isMTPDevice () const
{
	std::size_t found = this->Protocol.find("MTP");

	if (found == std::string::npos)
		return false;
	else
		return true;
}


/* -------------------------------------------------------------------------------------------------
Enumerate the content of the device, starting at the top (device's root).
This method only starts the enumeration process, by invoking the recursive function enumerate().
------------------------------------------------------------------------------------------------- */

HRESULT Device::enumerateContent (ScanMode sm)
{
	HRESULT hr;
	
	if (sm == ScanMode::Basic)
	{
		// Only get the root (depth level 1) and the Functional Objects (depth level 2); that are
		// things like the storage units, but may also be others (like 'Audio Capture', 'SMS', etc.).
		hr = enumerate(&tree.nodes, util::narrow(WPD_DEVICE_OBJECT_ID), 2, 0);
	}
	else if (sm == ScanMode::Full)
	{
		// Get all content (objects) on the device: Start level 0, max. depth unlimited (0).
		hr = enumerate(&tree.nodes, util::narrow(WPD_DEVICE_OBJECT_ID), 0, 0);
	}

	return hr;
}


/* -------------------------------------------------------------------------------------------------
Recursive function that walks the object hierarchy on a device to gather an overview of its content.

The hierarchy is usually:

My Computer
+ Device object                     (level 1)
  + Storage object X                (level 2)
    + Folder or file object         (level 3)
	  + More folder or file objects (level n)
	  + ...                         (...)
	+ ...                           (...)

Parameters:
	output   : Where the object data of the enumeration is stored.
	ObjectID : The starting point in the object hierarchy on the device from where to begin the enumeration.
	max_level: The maximal level of depth that the object hierarchy should be looked in; the default value of 0 means 'no limit, enumerate all'.
	cur_level: The current depth level of the enumeration process; the starting (and default) value is 0.
------------------------------------------------------------------------------------------------- */

// ****************
// *** ORIGINAL ***
// ****************
HRESULT Device::enumerate (std::vector<Object>* output, std::string ObjectID, int max_level, int cur_level)
{
	bool limit_depth;
	max_level == 0 ? limit_depth = false : limit_depth = true;
	
	if (cur_level < max_level || limit_depth == false)
	{
		HRESULT hr = S_OK;

		Object obj(this->device_properties, ObjectID);
		output->push_back(obj);

		// Enumerate children (if any) of provided object, one object at a time.
		IEnumPortableDeviceObjectIDs* id_enumeration = 0;
		hr = this->device_content->EnumObjects(0,                             // Flags (unused).
                                               util::widen(ObjectID).c_str(), // Starting from this object (parent).
                                               NULL,                          // Filter (unused).
                                               &id_enumeration);

		// Check if there is another object ID in the enumeration sequence and then collect it (and its children) by recursive call of this function.
		while (hr == S_OK)
		{
			LPWSTR obj_id        = NULL;
			ULONG  fetched_count = 0;
			
			hr = id_enumeration->Next(1,               // Number of objects to request on each 'Next' call (the MS sample code used 10).
                                      &obj_id,         // The destination (for multiple, this would need to be an array with n element (matching the number above)!
                                      &fetched_count);

            if (hr == S_OK)
            {
                hr = enumerate(output, util::narrow(obj_id), max_level, cur_level + 1);
            }

            CoTaskMemFree(obj_id); // OK?
		}

		// When no more children are available, S_FALSE is returned, which we promote back to S_OK to get the other objects.
		if (hr == S_FALSE)
			hr = S_OK;

		if (id_enumeration != NULL)
			id_enumeration->Release();

		return hr;
	}
	else
	{
		return S_FALSE;
	}
}


HRESULT Device::enumerate (std::vector<Tree::Node>* output, std::string object_id, int max_level, int level)
{
	bool limit_depth;
	max_level == 0 ? limit_depth = false : limit_depth = true;
	
	if (level < max_level || limit_depth == false)
	{
		HRESULT hr = S_OK;
		
		Tree::Node node;
		Tree::Node* parent_node = new Tree::Node;

        node.data = Object(this->device_properties, object_id);
		
		// To prevent crash.
		// OTOH, "H:" or "s10001" don't get identified as parents (due being functional objects...?)
		if (!node.data.getParentID().empty())
		{
			parent_node->data = Object(this->device_properties, node.data.getParentID());
			node.parent = parent_node;
		}
		else
		{
			node.parent = 0;
		}

		// If it's the first run of this recursive call and the label (display name or original file name) is empty,
		// set this top-level object's display name to the device's display name.
		if (level == 0 && node.data.getLabel().empty())
			node.data.setName(this->getFriendlyName());

		// Root at the very top: "DEVICE" (does not have a parent).
		//   Below that: Functional objects (for example of the 'Storage' category, e.g. "H:" or "s10001"), does not list any parent (not even "DEVICE").
		//     Below that: Top level folder (like H:\top). Its parent is itself (= H:\top).
		//       Below that: Further objects, like files or subfolders (like H:\top\file). Its parent would be H:\top.
		
		// Enumerate children (if any) of provided object, one object at a time.
		IEnumPortableDeviceObjectIDs* ID_Enumeration = 0;
		hr = this->device_content->EnumObjects(0, util::widen(object_id).c_str(), NULL, &ID_Enumeration);

		// Check if there is another object ID in the enumeration sequence and then collect it (and its children) by recursive call of this function.
		while (hr == S_OK)
		{
			LPWSTR next_object_id = NULL;
			ULONG  fetched_count  = 0;
			
			hr = ID_Enumeration->Next(1, &next_object_id, &fetched_count);

			if (hr == S_OK)
				hr = enumerate(&node.children, util::narrow(next_object_id), max_level, level+1);
		}

		output->push_back(node);
		
		// When no more children are available, S_FALSE is returned, which we promote back to S_OK to get the other objects.
		if (hr == S_FALSE)
			hr = S_OK;

		if (ID_Enumeration != NULL)
			ID_Enumeration->Release();

		return hr;
	}
	else
	{
		return S_FALSE;
	}
}


/*--------------------------------------------------------------------------------------------------
Recursive function to print the tree-like model of the content of the found devices.

Tried to make it use without the 'indentation_level' parameter, but that didn't pan out:
- A static variable in a member function is still only static to the function, not the class/object
  instance; meaning the indentation level would increase even we processed different devices in a
  loop (i.e. doesn't get rest to 0 when the new device starts).
- The indentation level was messed up: Even increasing before/after commands/loops didn't work out.
- Some corner/edge cases could have been worked around, but would make the code more complicated
  than needed.

So it remains: Using the parameter 'indentation_level' and set its default value to '0',
so the user does not need to care.
--------------------------------------------------------------------------------------------------*/

void Device::printTree (std::vector<Tree::Node> tn, int indentation_level /* Set to a default value in its declaration! */)
{
	for (std::vector<Tree::Node>::iterator i = tn.begin(); i != tn.end(); ++i)
	{
		std::string indent (indentation_level, '\t');

		std::cout << '\n';
		
		std::cout << indent << "Original File Name  : " << i->data.getOriginalFileName() << "\n";
		std::cout << indent << "Name                : " << i->data.getName() << '\n';
        std::cout << indent << "ID                  : " << i->data.getID() << '\n';
        std::cout << indent << "Persistent Unique ID: " << i->data.getPersistentUniqueID() << '\n';
		
		if (i->data.isFunctionalObject())
			std::cout << indent << "Type                : " << "Functional Object" << '\n';
		else if (i->data.isFolder())
			std::cout << indent << "Type                : " << "Folder" << '\n';
		else
			std::cout << indent << "Type                : " << "File" << '\n';

		std::cout << indent << "Relative path       : " << i->data.getRelativeStoragePath() << '\n';
		std::cout << indent << "Size (bytes)        : " + std::to_string(i->data.getSizeInBytes()) + "\n";
        std::cout << indent << "Created             : " << i->data.getCreationTimeAsString() << '\n';
        std::cout << indent << "Authored            : " << i->data.getAuthorTimeAsString() << '\n';
        std::cout << indent << "Modified            : " << i->data.getModificationTimeAsString() << '\n';
		
		if (i->parent != 0)
			std::cout << indent << "Parent              : " << i->parent->data.getOriginalFileName() << std::endl;
		else
			std::cout << indent << "Parent              : " << "(root)" << std::endl;

		printTree(i->children, indentation_level+1);
	}
};


/*!
Creates a new folder on the device, on the specified storage.

\param storage_name The friendly name of the storage unit, as displayed by the File Explorer, for example "Phone" or "Card".
\param folder_path: The name/path of the new folder(s).\n
                    Missing folders will be generated, i.e. if the actual parameter is "foo\\bar\\baz", a nested folder hierarchy will be generated.\n
					If "foo" already exists, the new folders "bar" (and "baz") will be created below it.\n
					If none of the folders exist yet, the complete hierarchy will be created,
					starting at the device's first storage (e.g. "Smartphone (Device)\Phone (Storage)").

\retval 0 on success
\retval 1 on failure

\todo Check wether ParentObjectID is a folder; there's Object::isFolder()...
*/

int Device::createFolder (std::string storage_name, std::string folder_path)
{
	int retval = 1;

	// Split string "folder1\\folder2\\folder3" into segments: [folder1] [folder2] [folder3]
	std::vector<std::string> path_components = util::splitPath(folder_path);

	// Combine all path components, except the final path component (std::prev()!), to a string that will
    // be used to get the object ID of the parent folder. If not available, use the storage root as parent.
	std::string parent_folder;
	
	for (std::vector<std::string>::iterator p = path_components.begin(); p != std::prev(path_components.end()); ++p)
	{
		parent_folder.append(*p).append("\\");
	}

	std::string ParentObjectID = getObjectID(storage_name, parent_folder);

	// If no match is found (because the full folder path does not yet exist, for example), use the given storage object to be the starting point.
	if (ParentObjectID.empty())
		ParentObjectID = getStorageObjectIDByName(storage_name);

	if (!ParentObjectID.empty())
	{
        std::string NewParentObjectID = ParentObjectID;

		for (std::vector<std::string>::iterator p = path_components.begin(); p != path_components.end(); ++p)
		{
			// Get the objects (i.e. files and folders) on this level, for comparison.
			std::vector<Object> objects;
			enumerate(&objects, NewParentObjectID, 2, 0); // or just 1, 0
			
			bool folder_exists(false);

			// Iterate over all objects on this level, to check if the folder exists already.
			for (std::vector<Object>::iterator o = objects.begin(); o != objects.end(); ++o)
			{
				if (*p == o->getName())
				{
					// [FIXME] When a new folder is created by hand/via File Explorer, the internal
					// and the display name (result of getName()) don't match most of the time!
					// Internal it might still be "New Folder" (compare with object properties).
					// [TODO] Using PUID...?
					folder_exists = true;
					NewParentObjectID = o->getID();
				}
			}

			// Folder does not exist yet; create now...
			if (folder_exists == false)
			{
				if (this->device != NULL)
				{
					HRESULT                hr = S_OK;
					IPortableDeviceValues* final_object_properties = nullptr;
			
					if (SUCCEEDED(hr))
					{
						// The properties that describe the object that will be created on the device.
						IPortableDeviceValues* object_values;

						HRESULT hr = CoCreateInstance(CLSID_PortableDeviceValues, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceValues, (void**) &object_values);
		
						if (SUCCEEDED(hr))
						{
							if (object_values != NULL)
							{
                                object_values->SetStringValue(WPD_OBJECT_PARENT_ID, util::widen(NewParentObjectID).c_str());
								object_values->SetStringValue(WPD_OBJECT_NAME, util::widen(*p).c_str());
								object_values->SetStringValue(WPD_OBJECT_ORIGINAL_FILE_NAME, util::widen(*p).c_str());
								object_values->SetGuidValue(WPD_OBJECT_CONTENT_TYPE, WPD_CONTENT_TYPE_FOLDER);
                                object_values->SetGuidValue(WPD_OBJECT_FORMAT, WPD_OBJECT_FORMAT_PROPERTIES_ONLY);

                                object_values->QueryInterface(IID_PPV_ARGS(&final_object_properties));
							}
						}
                        		
						PWSTR newly_created_object_id = NULL;

						hr = this->device_content->CreateObjectWithPropertiesOnly(final_object_properties, &newly_created_object_id);

						// Set the newly created folder's ID as the starting point for the next round (to facilitate nested folders).
						NewParentObjectID = util::narrow(newly_created_object_id);

						if (SUCCEEDED(hr))
							retval = 0;			

						CoTaskMemFree(newly_created_object_id);
					}
				}
			}
		}
	}

	return retval;
}


/*!
Creates a new folder on the device, on the specified storage.

\param folder_path: The path of the new folder(s), starting with a valid storage name,
                    e.g. "F:\\Foo\\Bar\\Baz" or "Phone\\Foo\\Bar\\Baz".\n
                    Missing folders will be generated, i.e. if the actual parameter is "foo\\bar\\baz", a nested folder hierarchy will be generated.\n
					If "foo" already exists, the new folders "bar" (and "baz") will be created below it.\n
					If none of the folders exist yet, the complete hierarchy will be created,
					starting at the device's first storage (e.g. "Smartphone (Device)\Phone (Storage)").

\retval 0 on success
\retval 1 on failure

\todo Check wether ParentObjectID is a folder; there's Object::isFolder()...

\todo Merge the createFolder() overloads, so that only this single-argument version remains.
*/

int Device::createFolder (std::string folder_path)
{
    auto [storage, relative_path] = util::splitStorageAndPath(folder_path);

    if (!(getStorageObjectIDByName(storage).empty() && relative_path.empty()))
    {
        return createFolder(storage, relative_path);
    }
    else
    {
        return 1;
    }
}


/*!
Empties a folder object, meaning its content (but not the folder itself) will be deleted.

\param storage_name         The name of the storage object on the device, e.g. "Phone" or "Card".
\param relative_folder_path A relative path to the object on this device that should be emptied,
                            starting with a valid storage name, e.g. "F:\\Music\\Album 1" or "Phone\\Music\\Album 2".
\param recursively          Controls whether all content below a folder-object should also be deleted; default value is `false`.

\retval 0 on success
\retval 1 on failure
*/

int Device::emptyFolder (std::string storage_name, std::string relative_folder_path, bool recursively)
{
    int status = 1;

    if (!(getStorageObjectIDByName(storage_name).empty()) && !(relative_folder_path.empty()))
    {
        Tree t = getAllObjectsInFolder(storage_name, relative_folder_path, false);
        
        for (auto i : t.nodes)
        {
            for (std::vector<Tree::Node>::iterator n = i.children.begin(); n != i.children.end(); ++n)
            {
                status = deleteObject(storage_name, n->data.getRelativeStoragePath(), recursively);
            }
        }
    }

    return status;
}


/*!
Empties a folder object, meaning its content (but not the folder itself) will be deleted.

\param folder_path An absolute path to the object on this device that should be emptied, starting with a valid storage name,
                   e.g. "F:\\Music\\Album 1" or "Phone\\Music\\Album 2".
\param recursively Controls whether all content below a folder-object should also be deleted; default value is `false`.

\retval 0 on success
\retval 1 on failure
*/

int Device::emptyFolder (std::string folder_path, bool recursively)
{
    int status = 1;
    auto [storage, relative_path] = util::splitStorageAndPath(folder_path);
    status = emptyFolder(storage, relative_path, recursively);
    return status;
}


/* -------------------------------------------------------------------------------------------------
Copy data from a source stream to a destination stream using the specified size as the temporary buffer size.

[TODO]
	- Check the IStream::Write() return values for things like 'STG_E_MEDIUMFULL' <https://msdn.microsoft.com/en-us/library/windows/desktop/aa380014%28v=vs.85%29.aspx>.
	- What about IStream::CopyTo() method instead of Write()? "[...] is equivalent to [...] ::Write, although IStream::CopyTo will be more efficient." <https://msdn.microsoft.com/en-us/library/windows/desktop/aa380038%28v=vs.85%29.aspx>
------------------------------------------------------------------------------------------------- */

HRESULT Device::StreamCopy (IStream* DestinationStream, IStream* SourceStream, DWORD TransferSize, DWORD* BytesTransfered)
{
	HRESULT hr = S_OK;

	// Allocate a temporary buffer (of optimal transfer size) for the read results.
	BYTE* ObjectDataBuffer = new (std::nothrow) BYTE[TransferSize];
    
	if (ObjectDataBuffer != NULL)
	{
		DWORD TotalBytesRead    = 0;
		DWORD TotalBytesWritten = 0;
		DWORD BytesRead         = 0;
		DWORD BytesWritten      = 0;

		// Read until the number of bytes returned from the source stream is 0, or an error occured during transfer.
		do
		{
			// Read object data from the source stream.
			hr = SourceStream->Read(ObjectDataBuffer, TransferSize, &BytesRead);
			if (FAILED(hr))
			{
				printf("! Failed to read %d bytes from the source stream, hr = 0x%lx\n", TransferSize, hr);
			}

			// Write object data to the destination stream
			if (SUCCEEDED(hr))
			{
				TotalBytesRead += BytesRead; // Calculating total bytes read from device for debugging purposes only

				hr = DestinationStream->Write(ObjectDataBuffer, BytesRead, &BytesWritten);

                if (FAILED(hr))
				{
					printf("! Failed to write %d bytes of object data to the destination stream, hr = 0x%lx\n", BytesRead, hr);
				}

				if (SUCCEEDED(hr))
				{
					TotalBytesWritten += BytesWritten; // Calculating total bytes written to the file for debugging purposes only
				}
			}
		}
		while (SUCCEEDED(hr) && (BytesRead > 0));

		if ((SUCCEEDED(hr)) && (BytesTransfered != NULL))
		{
			*BytesTransfered = TotalBytesWritten;
            printf("Total Bytes Read: %d; Total Bytes Written: %d\n", TotalBytesRead, TotalBytesWritten);
		}

		delete [] ObjectDataBuffer; // Remember to delete the temporary transfer buffer...
	}
	else
	{
		printf("! Failed to allocate %d bytes for the temporary transfer buffer.\n", TransferSize);
	}

	return hr;
}


/* -------------------------------------------------------------------------------------------------
Create a collection of object properties.

Before you can upload a resource to a device you need to collect some information about it.
Depending on the resource type you may need to collect more data (or properties) that describe the
resource (e.g. an audio or image file).
------------------------------------------------------------------------------------------------- */

IPortableDeviceValues* Device::createObjectProperties (std::string ParentObjectID, std::string FilePath, IStream* FileStream)
{
	IPortableDeviceValues* values;
	CoCreateInstance(CLSID_PortableDeviceValues, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceValues, (void**)&values);
	
    HRESULT hr;
	hr = values->SetStringValue(WPD_OBJECT_PARENT_ID, util::widen(ParentObjectID).c_str());
		
    // Set the WPD_OBJECT_SIZE by requesting the total size of the data stream.
    if (SUCCEEDED(hr))
    {
		STATSTG statstg = {0};
        hr = FileStream->Stat(&statstg, STATFLAG_NONAME);

		if (SUCCEEDED(hr))
        {
            hr = values->SetUnsignedLargeIntegerValue(WPD_OBJECT_SIZE, statstg.cbSize.QuadPart);
            if (FAILED(hr))
            {
                printf("! Failed to set WPD_OBJECT_SIZE, hr = 0x%lx\n",hr);
            }
        }
        else
        {
            printf("! Failed to get file's total size, hr = 0x%lx\n",hr);
        }
    }

    if (SUCCEEDED(hr))
    {
		// Set the WPD_OBJECT_ORIGINAL_FILE_NAME by splitting the file path into a separate filename.
        WCHAR FileBaseName [MAX_PATH] = {0};
        WCHAR FileExt      [MAX_PATH] = {0};
        
		if (_wsplitpath_s(util::widen(FilePath).c_str(), NULL, 0, NULL, 0, FileBaseName, ARRAYSIZE(FileBaseName), FileExt, ARRAYSIZE(FileExt)))
        {
            printf("! Failed to split the file path\n");    
        }

		std::string FileName(util::narrow(FileBaseName));
		FileName.append(util::narrow(FileExt));

        if (SUCCEEDED(hr))
        {
			hr = values->SetStringValue(WPD_OBJECT_ORIGINAL_FILE_NAME, util::widen(FileName).c_str());
            if (FAILED(hr))
            {
                printf("! Failed to set WPD_OBJECT_ORIGINAL_FILE_NAME, hr = 0x%lx\n", hr);
            }
        }

		// Set the WPD_OBJECT_NAME. The object name could be a more friendly name like "This Cool Song" or "That Cool Picture".
        if (SUCCEEDED(hr))
        {
            hr = values->SetStringValue(WPD_OBJECT_NAME, util::widen(FileName).c_str());
            if (FAILED(hr))
            {
                printf("! Failed to set WPD_OBJECT_NAME, hr = 0x%lx\n", hr);
            }
        }

        // .MP3 audio files
        if (util::narrow(FileExt) == std::string(".mp3"))
        {
            hr = values->SetGuidValue(WPD_OBJECT_CONTENT_TYPE, WPD_CONTENT_TYPE_AUDIO);
            if (FAILED(hr))
            {
                printf("! Failed to set WPD_OBJECT_CONTENT_TYPE, hr = 0x%lx\n", hr);
            }
            else
            {
                hr = values->SetGuidValue(WPD_OBJECT_FORMAT, WPD_OBJECT_FORMAT_MP3);
                if (FAILED(hr))
                {
                    printf("! Failed to set WPD_OBJECT_FORMAT, hr = 0x%lx\n", hr);
                }
            }
        }
    }

	return values;
}


/*!
Copies a file from the host to the device.

\param file_path          The full path to the source file that should be copied; e.g. "C:\\folder1\\folder2\\file.txt".
\param storage_name       The name of the storage object on the device, e.g. "Phone" or "Card".
\param destination_folder The path of the destination folder on the device; e.g. "Music\\Album No. 1".
                          This folder must already exist; if not, the function will fail.
\param ContentType        Is set to a default value of `WPD_CONTENT_TYPE_GENERIC_FILE` in the function's declaration.

\retval 0 on success
\retval 1 on failure

\warning Duplicates with the same name are possible!

\warning Example:
         1. copy(WPD.txt)
         2. copy(WPD.txt)

\warning Two files, both named 'WPD.txt', will be created on the device! (Note that this will not happen with folders.)

\warning Update: According to some 'internet research', the MTP specification doesn't put a restriction on
         the object's filename (it's just another property string) or on the real, underlying filesystem of the device.
         (See also https://github.com/libmtp/libmtp/blob/libmtp-1-1-16/README#L814-L820 for confirmation.)

\todo #1 Might have to include a check: if FILE_NAME like... append "(1)" or similar.

\todo #2 Also, check for existence, maybe control via option: if (FILE_NAME exists... && OVERWRITE...). Else, we may end up with a lot same-looking copies...
*/

int Device::copyFileToDevice (std::string file_path, std::string storage_name, std::string destination_folder, const GUID ContentType)
{
	int retval = 1;

	if (!storage_name.empty())
	{
        std::string destination_folder_object_id = getObjectID(storage_name, destination_folder);

		if (!destination_folder_object_id.empty())
		{
            HRESULT hr = S_OK;
			DWORD   OptimalTransferSize = 0;

			IStream* FileStream;
			IPortableDeviceDataStream* FinalObjectDataStream;
			IPortableDeviceValues* FinalObjectProperties;
			IStream* TempStream;

			// Open the file as an IStream (simplifies reading/writing).
			if (S_OK == SHCreateStreamOnFileW(util::widen(file_path).c_str(), STGM_READ, &FileStream))
			{
				// Get the required properties needed to properly describe the data being transferred to the device.
				FinalObjectProperties = createObjectProperties(destination_folder_object_id,
					file_path,
					FileStream);

				this->device_content->CreateObjectWithPropertiesAndData(FinalObjectProperties,
					&TempStream,          // Returned object data stream.
					&OptimalTransferSize,
					NULL);

				TempStream->QueryInterface(IID_PPV_ARGS(&FinalObjectDataStream));

				// Call helper function that copies the contents of a source stream into a destination stream.
				DWORD TotalBytesWritten = 0;

				StreamCopy(FinalObjectDataStream, // Destination.
					FileStream,            // Source.
					OptimalTransferSize,   // The optimal transfer buffer size specified by the driver.
					&TotalBytesWritten);   // The total number of bytes transferred from file to the device.

				// Let the driver know that the transfer is complete.
				FinalObjectDataStream->Commit(0);

				// Show the new ObjectID.
				PWSTR NewlyCreatedObjectID = NULL;

				hr = FinalObjectDataStream->GetObjectID(&NewlyCreatedObjectID);

				if (SUCCEEDED(hr))
				{
					retval = 0;
					printf("The file '%s' was transferred to the device.\nThe newly created object's ID is '%ws'\n", file_path.c_str(), NewlyCreatedObjectID);
				}
				else
				{
					retval = 1;
					printf("Error: Failed to get the newly transferred object's identifier from the device, hr = 0x%lx\n", hr);
				}

                FileStream->Release();
                FinalObjectDataStream->Release();

				CoTaskMemFree(NewlyCreatedObjectID);
			}
            else
            {
                printf("Error: Could not open file stream for '%ws'\n", file_path.c_str());
            }
		}
	}

	return retval;
}


/*!
Deletes (multiple) object(s) -- file or folder -- on the device.

\param storage_name The name of the storage object on the device, e.g. "Phone" or "Card".
\param paths        A collection of relative paths to the objects that should be deleted; e.g. "Music\\Album 1\\track1.mp3", "Music\\Album 1\\track2.mp3", ...
\param recursively  Controls whether all content below a folder-object should also be deleted; default value is false.

\todo There are ways to ask the device whether it supports recursive deletion or not; but currently I just assume it does.
      Maybe checking, if folder?
*/

void Device::deleteObjects (std::string storage_name,
                            std::vector<std::string> paths,
                            bool recursively /* default: false */)
{
	/* Note: IPortableDeviceContent::Delete() has the capability to delete multiple objects in one go,
	but getting possible resulting error codes (that are written in another IPortableDevicePropVariantCollection) is a pain.
	Thus this 'simplistic' approach, for now. */

	for (std::vector<std::string>::iterator pos = paths.begin(); pos != paths.end(); ++pos)
	{
		deleteObject(storage_name, *pos, recursively);
	}
}


/*!
Deletes a single object -- file or folder -- on the device.

\param path        An absolute path to the object on this device that should be deleted, starting with a valid storage name,
                   e.g. "F:\\Music\\Album 1" or "Phone\\Music\\Album 1\\Track 1.mp3".
\param recursively Controls whether all content below a folder-object should also be deleted; default value is false.
                   If `recursively` is set to `false`, and the folder is not empty, the call will fail!

\retval 0 on success
\retval 1 on failure
*/

int Device::deleteObject (std::string path, bool recursively /* default: false */)
{
    auto [storage, relative_path] = util::splitStorageAndPath(path);

    if (!(getStorageObjectIDByName(storage).empty() && relative_path.empty()))
    {
        return deleteObject(storage, relative_path, recursively);
    }
    else
    {
        return 1;
    }
}


/*!
Deletes a single object -- file or folder -- on the device.

\param storage_name The name of the storage object on the device, e.g. "Phone" or "Card".
\param path         A relative path to the object on this device that should be deleted; e.g. "Music\\Album 1\\track1.mp3".
\param recursively  Controls whether all content below a folder-object should also be deleted; default value is false.
                    If `recursively` is set to `false`, and the folder is not empty, the call will fail!

\retval 0 on success
\retval 1 on failure

\todo More error codes?

\todo There are ways to ask the device whether it supports recursive deletion or not; but currently I just assume it does.
      Maybe checking, if folder?
*/

int Device::deleteObject (std::string storage_name,
                          std::string path,
                          bool recursively /* default: false */)
{
	HRESULT hr;
	PROPVARIANT pv;

	IPortableDevicePropVariantCollection* ObjectToDelete;
	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDevicePropVariantCollection, (void**)&ObjectToDelete);

    IPortableDevicePropVariantCollection* Results;
	hr = CoCreateInstance(CLSID_PortableDevicePropVariantCollection, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDevicePropVariantCollection, (void**)&Results);

	if (SUCCEEDED(hr))
	{
		std::string ObjectID = getObjectID(storage_name, path);

		if (ObjectID.empty())
		{
			std::cout << "Error: ID is empty for object '" << path << "' on '" << storage_name << "'!" << std::endl;
            return 1;
		}
		else
		{
            pv = { 0 };
			PropVariantInit(&pv);
			pv.vt = VT_LPWSTR;
			std::wstring str(util::widen(ObjectID)); // Trying to simplify these two lines didn't pan out yet. [FIXME]
			pv.pwszVal = &str[0];

			if (pv.pwszVal != NULL)
				hr = ObjectToDelete->Add(&pv);

			if (SUCCEEDED(hr))
			{
                HRESULT delete_hr = this->device_content->Delete((recursively ? PORTABLE_DEVICE_DELETE_WITH_RECURSION
                                                                              : PORTABLE_DEVICE_DELETE_NO_RECURSION),
                                                                              ObjectToDelete,
                                                                              &Results); //nullptr);

                if (delete_hr == S_OK)
                    return 0;
                    
                if (delete_hr == S_FALSE)
                {
                    std::cout << "Warning: At least one object could not be deleted." << std::endl;
                        
                    DWORD count = 0;
                    hr = Results->GetCount(&count);

                    for (DWORD index = 0; index < count; ++index)
                    {
                        PROPVARIANT error = {0};
                        PropVariantInit(&error);
                        hr = Results->GetAt(index, &error);
                        
                        if (SUCCEEDED(hr) && (error.vt == VT_ERROR))
                        {
                            switch (error.scode)
                            {
                                case 0x80070091:
                                    std::cout << "Error: '" << path << "' on '" << storage_name << "' - The directory is not empty (0x" << std::hex << error.scode << std::dec << ")" << std::endl;
                                    break;

                                default:
                                    std::cout << "Error: '" << path << "' on '" << storage_name << "' - error code 0x" << std::hex << error.scode << std::dec << std::endl;
                            }
                        }
                        PropVariantClear(&error);
                    }
                        
                    return 1;
                }
                    
                if (delete_hr == E_ACCESSDENIED)
                {
                    std::cout << "Error: Access denied for object '" << path << "' on '" << storage_name << "'." << std::endl;
                    return 1;
                }

                if (delete_hr == HRESULT_FROM_WIN32(ERROR_DIR_NOT_EMPTY))
                {
                    std::cout << "Error: Object '" << path << "' on '" << storage_name << "' could not be deleted because it was not empty." << std::endl;
                    return 1;
                }

                if (delete_hr == HRESULT_FROM_WIN32(ERROR_INVALID_OPERATION))
                {
                    std::cout << "Error: Invalid Operation for object '" << path << "' on '" << storage_name << "'." << std::endl;
                    std::cout << "       Hint: Maybe you specified 'no recursion', but the object has children." << std::endl;
                    return 1;
                }

                if (delete_hr == HRESULT_FROM_WIN32(ERROR_NOT_FOUND))
                {
                    std::cout << "Error: Object '" << path << "' on '" << storage_name << "' was not found." << std::endl;
                    return 1;
                }

                if (delete_hr == HRESULT_FROM_WIN32(ERROR_BUSY))
                {
                    std::cout << "Error: Busy - The requested resource is in use." << std::endl;
                    return 1;
                }
			}

            PropVariantClear(&pv);	
		}

        return 1;
	}
    else
    {
        return 1;
    }
}


/*!
Renames an object (file or folder) on the device.

\param storage_name            The storage of the device where this function should look for the object; e.g. "Phone" or "Card".
\param relative_path_to_object The relative path to the storage on the device; e.g. "Music\\Folder 1\\file.mp3" or "Music\\Folder 1\\Folder 2".
\param new_name                The new name of the object; e.g. "file_new.mp3" or "Folder 2 (New)".
		
\warning Caveat: MTP is a fragile and fickle thing!
         My first attempt with a Samsung S4mini (Android 4.2.2):
         Only the folder objects changed its WPD_OBJECT_ORIGINAL_FILE_NAME and WPD_OBJECT_NAME (the display name in File Exlorer),
         the file object only changed it's WPD_OBJECT_ORIGINAL_FILE_NAME and kept the old WPD_OBJECT_NAME.
         Trying again and again, re-connecting the device etc. didn't make any difference.\n
		 \n
         Then I tested it with a Samsung A3 (Android 5.1.1):
         First run, again no problem with the folder renaming, but the file just got copied with a .dup0 suffix (happend on the
         other device as well, once).
         But then, with the second and following run(s), all was well: Both properties for folder and file changed.\n
		 \n
         Addendum: Seems the '.dup0' suffix is something like '(Copy 1)' on a normal Windows system and gets appended if (at least)
         the WPD_OBJECT_ORIGINAL_FILE_NAME already exists... maybe deal with it, later.\n
         \n
         And then I tried it again with the Samsung S4mini (Android 4.2.2) and behold, now suddenly, both properties
         for both object types were changed here too -- WTF?!

\note Omitted proper error checking.
*/

HRESULT Device::renameObject (std::string storage_name, std::string relative_path_to_object, std::string new_name)
{
	HRESULT r  = S_FALSE;
	
	if (!storage_name.empty())
	{
		std::string ObjectID = getObjectID(storage_name, relative_path_to_object);

		if (!ObjectID.empty())
		{
			if (supportsCommand(WPD_COMMAND_OBJECT_PROPERTIES_SET) == false)
				return S_FALSE;

			IPortableDeviceValues* properties, * values, * results;
			r = CoCreateInstance(CLSID_PortableDeviceValues, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceValues, (void**)&properties);
			r = CoCreateInstance(CLSID_PortableDeviceValues, NULL, CLSCTX_INPROC_SERVER, IID_IPortableDeviceValues, (void**)&values);

			// The GUID part is used to indicate the category that the command belongs to (that is, related commands
			// belong to the same category, therefore will have the same 'fmtid').
			r = properties->SetGuidValue(WPD_PROPERTY_COMMON_COMMAND_CATEGORY, WPD_COMMAND_OBJECT_PROPERTIES_SET.fmtid);

			// The DWORD part indicates the command ID, and is used to distinguish the individual commands within a
			// command category (that is, the 'pid' values for commands in the same category will be different).
			r = properties->SetUnsignedIntegerValue(WPD_PROPERTY_COMMON_COMMAND_ID, WPD_COMMAND_OBJECT_PROPERTIES_SET.pid);

			r = properties->SetStringValue(WPD_PROPERTY_OBJECT_PROPERTIES_OBJECT_ID, util::widen(ObjectID).c_str());
			r = values->SetStringValue(WPD_OBJECT_ORIGINAL_FILE_NAME, util::widen(new_name).c_str());
			r = values->SetStringValue(WPD_OBJECT_NAME, util::widen(new_name).c_str());
			r = properties->SetIPortableDeviceValuesValue(WPD_PROPERTY_OBJECT_PROPERTIES_PROPERTY_VALUES, values);

			r = device->SendCommand(0, properties, &results);
		}
	}

	return r;
}


/*!
Returns the ObjectID of the leaf of a given path (relative to the given storage object or the root of the device).

\param storage_name Name of a storage object on the device (e.g. "Phone" or "Card") that should be used as the the starting point.
                    If the argument is empty (""), the root of the device is used as the starting point.
\param path         The path, relative to the starting point object on the device.

\return The Object ID as a string.

\todo Returns only the last match: Loops over the objects and overwrites the found matches; so only the last one survives.
      Not ideal when you have a similar structure/names on multiple storages:
      - device\stor1\path\file.ext
      - device\stor2\path\file.ext <- only this ID will be returned.
      Alternatives?
      Not using a _relative_ storage path maybe?

\todo Performance: Enumerating the whole content on each call slows everything down; just re-enumerate when
      something significant happens (like object addition/deletion/renaming/etc.). Signal?
*/

std::string Device::getObjectID (std::string storage_name, std::string path)
{
	std::string         ObjectID;
	std::vector<Object> current_object_collection;
	
	if (!path.empty())
	{
		// Use either the device's root or a specific storage object as the starting point.
		std::string start_object_id = storage_name.empty() ? util::narrow(WPD_DEVICE_OBJECT_ID) : getStorageObjectIDByName(storage_name);
		
		// Recursive enumeration, starting at the given object.
		enumerate(&current_object_collection, start_object_id);

		// Iterate over each item of the vector and compare with the available content.
		for (std::vector<Object>::iterator pos = current_object_collection.begin(); pos != current_object_collection.end(); ++pos)
		{
            if (path == pos->getRelativeStoragePath())
				ObjectID = pos->getID();
		}
	}

	return ObjectID;
}


/*!
Checks whether an object exists on the device/storage.

\param storage_name Name of a storage object on the device (e.g. "Phone" or "Card") that should be used as the the starting point.
                    If the argument is empty (""), the root of the device is used as the starting point.
\param path         The path, relative to the starting point object on the device.

\retval true  if the object is found.
\retval false if the object is not found.
*/

bool Device::existsObject (std::string storage_name, std::string path)
{
	if (getObjectID (storage_name, path).empty())
		return false;
	else
		return true;
}


/* -------------------------------------------------------------------------------------------------
Checks whether the device supports a certain command (WPD_COMMAND_*)
Note: Omitted checking for errors (return values).
------------------------------------------------------------------------------------------------- */

bool Device::supportsCommand (PROPERTYKEY command_key)
{
	IPortableDeviceCapabilities*  capabilities;
	IPortableDeviceKeyCollection* commands;
	unsigned long                 number_of_commands = 0;
	PROPERTYKEY                   key = WPD_PROPERTY_NULL;

	this->device->Capabilities(&capabilities);
	capabilities->GetSupportedCommands(&commands);
	commands->GetCount(&number_of_commands);
	
	for (unsigned int index = 0; index < number_of_commands; index++)
	{
		commands->GetAt(index, &key);

		if (IsEqualPropertyKey(command_key, key))
			return true;
	}
	return false;
}


/* -------------------------------------------------------------------------------------------------

~~~ WORK IN PROGRESS / DESIGN PHASE ~~~

Argument: A path of the directory (or file) we're interested in, in text form.

The Windows file explorer for example displays "My Computer\Sascha Offe (GT-I9195)\Phone\Pictures\Screenshots".

Omit the base shell object ('My Computer') and name of the device ('Samsung Galaxy S4 mini (GT-I9195)')
from the path argument. Those are known (CLSID) respectively can be deduced from the current device object's
properties; only specify the rest of the path: "Phone\Pictures\Screenshots".

Returns a path that consists of persistent unique IDs (PUIDs) that can be passed for example to
ShellExecute(0, _T("explore"), PUID_Path, ...) to open a file explorer window with that folder.

  ::{20D04FE0-3AEA-1069-A2D8-08002B30309D}\                                                 -> The shell namespace ID for 'My Computer'.
+ \\\?\USB#VID_04E8&PID_6860&MI_00#7&3826ceb&0&0000#{6ac27878-a6fa-4155-ba85-f98f491d4f33}\ -> The Plug'n'Play Device ID for 'Sascha Offe (GT-I9195)'.
+ SID-{10001,SECZ9519043CHOHB,5793759232}\ (... or simply use the name, 'Phone'?)           -> The ID for the storage entity 'Phone'.
+ ...\                                                                                      -> The persistent unique ID for the folder object 'Pictures'.
+ ...\                                                                                      -> The persistent unique ID for the folder object 'Screenshots'.
+ ...                                                                                       -> The persistent unique ID for the file object 'image.png'.

------------------------------------------------------------------------------------------------- */

/*
std::wstring getPUIDPathFromTextPath (std::wstring Path)
{
	std::wstring PUIDPath;
	PUIDPath.append(_T("::{20D04FE0-3AEA-1069-A2D8-08002B30309D}")).append(_T("\\")).append(this->PnPDeviceID).append(_T("\\"));

	// Split path into components.
	std::vector<std::wstring> PathComponents;
	wchar_t           c = L'\\';
	string::size_type i = 0;
	string::size_type j = Path.find(c);

	while (j != string::npos)
	{
		PathComponents.push_back(Path.substr(i, j-i));
		i = ++j;
		j = Path.find(c, j);

		if (j == string::npos)
			PathComponents.push_back(s.substr(i, Path.length()));
	}

	// Iterate over the path components and check/append.
	for (std::vector<std::wstring>::iterator pos = PathComponents.begin(); pos != PathComponents.end(); ++pos)
	{
		// TODO: if pos exists (find in enumerated objects list)
			PUIDPath.append(pos->PersistentUniqueID).append(_T("\\"));
		else:
			break;
	}

	return PUIDPath;
}

*/


/*--------------------------------------------------------------------------------------------------
Get the Plug & Play Device ID (a.k.a. the "Device Instance Path").
*/

std::string Device::getID () const
{
	return this->PnPDeviceID;
}


/*--------------------------------------------------------------------------------------------------
Get the Plug & Play Device ID (a.k.a. the "Device Instance Path") as a zero-terminated C-string.
*/

void Device::getID (char* destination, size_t n) const
{
	/* In case the source string does not have a terminating zero/NUL character ('\0') within
	the 'n' bytes that strncpy() is limited to copy, then strncpy() will pad the destination buffer
	with '\0' only if the source is smaller than 'n'!
	Else (source is bigger than 'n') we should zero-terminate it manually at the end, to generate a
	proper zero-terminated C-string. */

	strncpy(destination, this->PnPDeviceID.c_str(), n-1);
	destination[n-1] = '\0';
}


/*--------------------------------------------------------------------------------------------------
Get the storage object at the provided i.
--------------------------------------------------------------------------------------------------*/

Storage Device::getStorage (int index)
{
	return this->storages.at(index);
}


/*--------------------------------------------------------------------------------------------------
Get the storage object by the provided name.
--------------------------------------------------------------------------------------------------*/

Storage Device::getStorage (std::string name)
{
	for (std::vector<Storage>::iterator pos = storages.begin(); pos != storages.end(); ++pos)
	{
		if (name == pos->getName())
			return *pos;
	}
}


/*--------------------------------------------------------------------------------------------------
Get the Persistent Unique ID (PUID) of the storage unit of the given i.

Note that at() will throw an out_of_range std::exception if i is not within bounds.
--------------------------------------------------------------------------------------------------*/

std::string Device::getStoragePUID (unsigned int index)
{
	return this->storages.at(index).getPersistentUniqueID();
}


/*!
Get the ObjectID of the storage unit with given name.

\param name Name of the storage object, e.g. "Card".

\return A string that either represents the ID (e.g. "s10001" for "Phone", or "s20002" for "Card");
        or an empty string (""), if no match is found.
*/

std::string Device::getStorageObjectIDByName (std::string name)
{
	for (std::vector<Storage>::iterator i = storages.begin(); i != storages.end(); ++i)
	{
		if (i->getName() == name)
			return i->getObjectID();
	}

	return "";
}


/*--------------------------------------------------------------------------------------------------
Get the name of the storage unit of the given i.

Note that at() will throw an out_of_range std::exception if i is not within bounds.
--------------------------------------------------------------------------------------------------*/

std::string Device::getStorageName (int storage_index) const
{
	return this->storages.at(storage_index).getName();
}


void Device::getStorageName (int storage_index, char* destination, size_t n) const
{
	/* In case the source string does not have a terminating zero/NUL character ('\0') within
	the 'n' bytes that strncpy() is limited to copy, then strncpy() will pad the destination buffer
	with '\0' only if the source is smaller than 'n'!
	Else (source is bigger than 'n') we should zero-terminate it manually at the end, to generate a
	proper zero-terminated C-string. */

	strncpy(destination, this->storages.at(storage_index).getName().c_str(), n-1);
	destination[n-1] = '\0';
}


/*--------------------------------------------------------------------------------------------------
Get the count of detected storage units on the device.
--------------------------------------------------------------------------------------------------*/

int Device::getStorageCount () const
{
	return storages.size();
}


/*--------------------------------------------------------------------------------------------------
Get the 'friendly name' of the device.
--------------------------------------------------------------------------------------------------*/

std::string Device::getFriendlyName () const
{
	return this->FriendlyName;
}


void Device::getFriendlyName (char* destination, size_t n) const
{
	/* In case the source string does not have a terminating zero/NUL character ('\0') within
	the 'n' bytes that strncpy() is limited to copy, then strncpy() will pad the destination buffer
	with '\0' only if the source is smaller than 'n'!
	Else (source is bigger than 'n') we should zero-terminate it manually at the end, to generate a
	proper zero-terminated C-string. */

	strncpy(destination, this->FriendlyName.c_str(), n-1);
	destination[n-1] = '\0';
}


void Device::getManufacturer (char* destination, size_t n) const
{
	strncpy(destination, this->Manufacturer.c_str(), n-1);
	destination[n-1] = '\0';
}


void Device::getModel (char* destination, size_t n) const
{
	strncpy(destination, this->Model.c_str(), n-1);
	destination[n-1] = '\0';
}

void Device::getSerialNumber (char* destination, size_t n) const
{
	strncpy(destination, this->SerialNumber.c_str(), n-1);
	destination[n-1] = '\0';
}


void Device::getFirmwareVersion (char* destination, size_t n) const
{
	strncpy(destination, this->FirmwareVersion.c_str(), n-1);
	destination[n-1] = '\0';
}


void Device::getProtocol (char* destination, size_t n) const
{
	strncpy(destination, this->Protocol.c_str(), n-1);
	destination[n-1] = '\0';
}


void Device::getType (char* destination, size_t n) const
{
	std::string type = translateDeviceType(this->Type);

	strncpy(destination, type.c_str(), n-1);
	destination[n-1] = '\0';
}


void Device::getPowerSource (char* destination, size_t n) const
{
	strncpy(destination, (this->PowerSource == 0 ? "Battery" : "External"), n-1);
	destination[n-1] = '\0';
}


void Device::getPowerLevel (char* destination, size_t n) const
{
	std::string powerlevel = util::NumberToString(this->PowerLevel);
		// This temporary variable is needed. Directly invoking NumberToString() in 'new' and strcpy() causes the program to crash.
	
	strncpy(destination, powerlevel.c_str(), n-1);
	destination[n-1] = '\0';
}


/*--------------------------------------------------------------------------------------------------
Get the drive letter of the device's storage.
Returns the null character (NUL or '\0') if no valid drive letter was found, else the alphabetic
character (the case depends on what data the storage returns).
--------------------------------------------------------------------------------------------------*/

char Device::getDriveletter () const
{
	/* If the device is not a MSC Mass-Storage-Class device (e.g. a normal USB thumbdrive), it will
	   not have a drive letter assigned, so we can skip this: MTP-protocol devices like smartphones
	   don't use drive letters. */
	if (this->isMSCDevice())
	{
		// Again, just a presumption: These drives usually only have one/primary partition/volume.
		// Use the first letter of the Friendly Name string (which would be something like "F:").
		char letter = this->storages.at(0).getName().at(0);

		/* Check if the given character is an alphabetic character as classified by the currently
		   installed C locale (does not matter whether it's uppercase or lowercase).
		   The argument should first be converted to unsigned char, to be used with plain chars. */
		if (std::isalpha(static_cast<unsigned char>(letter)))
			return letter;
		else
			return '\0';
	}

	return '\0';
}


/*--------------------------------------------------------------------------------------------------
Refreshes (updates, reloads) misc. data and information related to the device and its objects
[TODO] Rename to something better: refreshInfo()?
--------------------------------------------------------------------------------------------------*/

void Device::refresh ()
{
	// Walk over all storage objects and let it update.
	for (std::vector<Storage>::iterator i = storages.begin(); i != storages.end(); ++i)
	{
		i->updateFreeBytesCount();
	}
}


/*--------------------------------------------------------------------------------------------------
Returns a 'Tree' object that contains all the objects (files, folders, etc.) found in (and below,
if the parameter 'recusive' is true) a specified folder [object] on a specific storage unit.
--------------------------------------------------------------------------------------------------*/

Tree Device::getAllObjectsInFolder (std::string StorageObjectName, std::string PathToFolder, bool recursive /* See default value in declaration! */)
{
	Tree output_tree;

    Object obj(this->device_properties, getObjectID(StorageObjectName, PathToFolder));

    if (obj.isFolder())
    {
        int depth = (recursive ? 0 : 2);
            // '0' means 'unlimited': Follow all descendant objects (files and subfolders).
            // '2' means 'follow only two levels': The start folder object itself and its direct descendants.

        enumerate(&output_tree.nodes, obj.getID(), depth);
    }

    return output_tree;
}


/* Relay the named function to handle the named event type of the device. */

void Device::setEventCallback (DeviceEventType e, std::function<void()> callback)
{
	device_event->setEventCallback(e, callback);
}

} // namespace WPDLib
