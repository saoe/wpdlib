﻿/*
╭──────────────────────────────────────────────────────────────────────────────────────────────────╮
│                            Windows Portable Devices Library (WPDLib)                             │
╰──────────────────────────────────────────────────────────────────────────────────────────────────╯
Copyright © 2018 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT
*/

#define DLL_EXPORT

#include "Tree.h"
#include <iostream>


namespace WPDLib
{

/* -------------------------------------------------------------------------------------------------
Get the amount of children (nodes) of a tree; without counting the root-parent.

[FIXME] Not counting as intented: Hit 'Scan Device' again & again and watch the number grow...!
[IDEA] Put this into Device::enumerate()?
[IDEA] Rename? countNodes, ...?

------------------------------------------------------------------------------------------------- */

int Tree::countChildren (std::vector<Node> n, int previous_count) const
{
	int counter = previous_count;
	int total = 0;

	//std::cout << "counter: " << counter << '\n';
	
	for (std::vector<Node>::const_iterator i = n.begin(); i != n.end(); ++i)
	{
		++counter;
		std::cout << "#" << counter << " - "  << i->data.getLabel() << "\n";
		total += countChildren(i->children, counter) ;//+ counter;
	}

	std::cout << "current counter: " << counter << " (total: " << total << ")\n";

	return total - 1;    // Minus one, because the very first node is the root ('super-parent'), not really a child.
}

} // namespace WPDLib.