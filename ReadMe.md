# WPDLib — Windows Portable Devices Library

1. Overview
2. Getting Started
3. Usage
4. References

----------------------------------------------------------------------------------------------------

## 1 Overview

WPDLib is a dynamic-link-library for Microsoft Windows to be used by C++ programs
to access MTP devices (such as Android Smartphones and media players), as well as
USB mass-storage devices (like flash memory stick or plain MP3 players).
It uses Microsoft's Windows Portable Devices (WPD) API.

The (current) raison d’être of this project is to enable my "RandFill" program
to communicate with modern devices.

Note that this library has been developed for my own needs, thus it is not
supposed to be an universal, full-fledged solution to all the needs you
might have.

### 1.1 History

This project is a spin-off from my "RandFill" project, a Windows program
for filling portable music players with randomly chosen files from a
local music archive.

"RandFill" only knew how to handle Mass Storage Class (MSC) devices,
also known as USB Mass Storage (UMS) devices.
That was fine a while ago, but nowadays most people use smartphones
as their MP3/media player and recent versions of Android and Windows
Phone expose their storage only over the the Media Transport Protocol (MTP).
(I believe Apple iOS devices use use a completely different method...?)

With its Windows Portable Devices (WPD) API, Microsoft Windows can
communicate with both device types: The older MSC/UMS devices (like
portable flash memory, MP3 players and external hard drives), as well
as the newer MTP devices (smartphones and other media players).

By starting from a clean slate (new project, stand-alone DLL), I hope
it will be easier than fiddling with the current device-handling codebase
of "RandFill".

### 1.2 Supported Platforms

- This project is created and tested on/for Microsoft Windows as its
  primary platform in mind.
- Development began on Windows 7 and happens now on Windows 10;
  currently no longer tested with Windows 7.
- Windows XP requires Windows Media Player 10 or higher for WPD support;
  later Windows versions have built-in support.
  (Just an information snippet! WPDLib was never built or tested under Windows XP!)

### 1.3 Caveats

A quote from the [MTP page on the ArchLinuxWiki](https://wiki.archlinux.org/index.php/Media_Transfer_Protocol),
and even though the page is about Linux and not Windows, the _Note_ fits perfectly:

> Note: MTP is messy and its implementation varies between devices.

Oh. Yes. Hell, yeah.

A few insights from my time working with this _shitty_ protocol (or maybe simply _shitty_ implementations of it):

1. MTP is a fragile, fickle thing and higly sequential in it's working: No parallel access or operations
   are allowed; one can't edit/modify files (object) in place, it's just meant for transfer.

2. The devices and/or drivers don't need to (and usually don't) implement the whole specification;
   don't trust a command to be working as expected or a capability even to be available just because
   it says so in the MTP specs. Some things you would assume/expect to work only do so halfway, on
   one device, but maybe better on the next...

3. WPD doesn't seem to be a hot topic, never was: (non-official) documentation, examples, etc. are
   hard to come by and pretty old by now. Be prepared for searching, digging and hitting your head
   on the desk long, hard and often.

   If you're lucky, you may find a post that at least confirms that you're not alone with this and
   not totally stupid.
   ([an example, which I also experienced](https://stackoverflow.com/questions/42648329/windows-portable-device-notify-when-a-new-file-is-created-copied-deleted))

4. Lately, I hit again walls. I now plugged in a little zoo of smartphones for testing; different
   manufacturers, different Android versions. And of course, some stuff works on the first device,
   but not the second and maybe only half on the third.
   
   It's fucking frustrating, because I don't have the slightest clue, why that is happening: "Is my
   code for function X crap? But why then does it work on device Y? And my code looks like the sample
   I could find on the internet, one even from Microsoft..." -- and so on.
   
   A while ago, when I was browsing again for solutions, or at least answers or explanations, I stumbled
   accross an open-source repository that maintained a looong list of 'switch-cases' for hundreds of device IDs.
   I won't (can't and don't want to) do something like that.
   And even if I would: I'd currently not even know how to workaround some of the issues :-(

### 1.4 Plans

(see [Tracker.md](Tracker.md) for a kind of Todo list)

----------------------------------------------------------------------------------------------------

## 2 Getting Started

Currently, the library is not available as a pre-compiled binary, so if you want to play with it,
you'd need to build it from source.

### 2.1 Requirements, preferred tools and dependencies

- Build tool: [CMake](http://www.cmake.org)
- Compiler: [Microsoft Visual Studio](https://visualstudio.microsoft.com/)
  (tested with version 2017/15.x or 2019/16.x) with the Windows 10 SDK
  (install the "Desktop development with C++" workload).
- Optional: The [Qt](https://www.qt.io/) framework, if you want to build the QtGUI-TestApp.
- Optional: [Doxygen](https://www.doxygen.nl/), if you want to generate the API documentation.

### 2.2 Configure

Note that you may have to edit or provide some other values to suit your specific environment!

1. Open a command line prompt in the top-level directory of your project:

		C:\>cd path\to\the\project

2. Let CMake configure and generate the native build tool files.

   Example:
   
       cmake -D CMAKE_PREFIX_PATH=C:\devel\ext\Qt\5.14.0\_64DLL\lib\cmake\Qt5 -D CMAKE_INSTALL_PREFIX=C:\devel\int -S src -B _build -G "Visual Studio 16 2019" -A x64
   
   Parameters (note that definitions (`-D`) must come first on the command line!):
   
   | Option                      | Description
   |-----------------------------|-----------------------------------------------------------------------------------
   | -D CMAKE_PREFIX_PATH=...    | Paths (seperated by semicolons) to the CMake Config files of the external dependecies like _Qt_ (i.e. Qt5Config.cmake, Qt5ConfigVersion.cmake, Qt5ModuleLocation.cmake) and _WPDLib_.
   | -D CMAKE_INSTALL_PREFIX=... | Optional. Path to the destination folder for installation (used by target _INSTALL_); see below for details.
   | -S _\<Path\>_               | Path to the source code files and the top-level CMakeLists.txt (available since CMake 3.13).
   | -B _\<Path\>_               | Path to the out-of-source build directory (available since CMake 3.13).
   | -G _\<Generator\>_          | Optional: Name of the generator (e.g. `Visual Studio 16 2019`). If you omit this, CMake will use the default generator.
   | -A _\<Platform\>_           | Name of the target architecture/platform (if supported by the generator, e.g. `Win32` or `x64` for MSVC).

### 2.3 Build

This step will create the DLL and import-LIB file (amongst other things).

- either use CMake (shortcut to call the native tools, like Visual C++); example:

		cmake --build _build --target ALL_BUILD --config Release --parallel 2

	Note: Since CMake 3.12, the -j [\<jobs\>] resp. --parallel [\<jobs\>] switch works for all tools (incl. MSBuild).  
	The value for 'jobs' is the maximum number of concurrent processes/projects to use when building.
	If 'jobs' is omitted the native build tool’s default number is used.

- or use the Visual Studio IDE by opening the generated `*.sln` file in the build directory.

### 2.4 Install

This will copy the library, header files etc. to the specified destination.

a. Either build the _install_ target...

   This will also build all required dependencies, since the target _INSTALL_ depends on the target _ALL_BUILD_, so you could skip the build step (4.3).

   Example:
	  
	cmake --build _build --target install --config Release

b. Or use the `--install` switch as a shortcut (available since CMake 3.15)

   With the optional(!) `--prefix`, you can override the value for `CMAKE_INSTALL_PREFIX` set in the configuration/generation step,
   or set the value now, if you forgot to supply it in the configuration step earlier.  
     
   Note: For this to work, the explicit build step (4.3) is required before, otherwise the target _INSTALL_ target will not exist!
   
   Example:
	  
	cmake --install _build --prefix C:\devel\int

----------------------------------------------------------------------------------------------------

## 3 Usage

### 3.1 Incorprate it as a package in your own project's CMakeLists.txt

	project(test)                   # Must come before find_package()!
	find_package(WPDLib REQUIRED)
	add_executable(foo bar.cpp)
	target_link_libraries(foo WPDLib::Library)

Point the project to the WPDLibConfig.cmake (etc.) files by setting CMAKE_PREFIX_PATH:

	cmake -D CMAKE_PREFIX_PATH=C:\<the_path>\<to_your>\<installation_of>\WPDLib\cmake -S <YourSrcDir> -B <YourBuildDir>
	cmake --build <YourBuildDir>

### Example source code:

_[TODO]_ (Examples, Tutorials, FAQ)

----------------------------------------------------------------------------------------------------

## 4 References

Just some hints. There are not so many helpful articles or tutorials out there; and most of few are
rather old. Example code is also spread thin over the internet. Not much love for WPD out there, it
seems...

1. [MSDN: WPD Application Programming Interface](https://msdn.microsoft.com/en-us/library/windows/desktop/dd389005%28v=vs.85%29.aspx)
2. Windows SDK: Samples: Multimedia: WPD
3. 'Calibre' source code  
4. [Blog from _dimeby8_ on Microsoft](https://blogs.msdn.microsoft.com/dimeby8/tag/wpd/); content from 2006/2007.
5. [Christophe Geers' Blog about .NET and C# programming](https://cgeers.wordpress.com/category/programming/wpd/)  
   Focus is on WPD with C#, but still useful for understanding and examples; content is from 2011/2012.

----------------------------------------------------------------------------------------------------

Copyright © 2015-2020 Sascha Offe <so@saoe.net>  
SPDX-License-Identifier: MIT
